import 'package:adira_one_partner/services/login_change_notifier.dart';
import 'package:adira_one_partner/utils/constanta.dart';
import 'package:adira_one_partner/utils/size_config.dart';
// import 'package:adira_one_partner/db_helper/database_helper.dart';
//import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  // @override
  // void initState() {
  //   super.initState();
  //   Provider.of<LoginChangeNotifier>(context, listen: false).setRandomText(0);
  // }

  Screen size;

  //new
  double getWidhtScreen(BuildContext context) =>
      MediaQuery.of(context).size.width;

  @override
  Widget build(BuildContext context) {
    //new
    double smallDiameter = getWidhtScreen(context) * 0.6;
    double bigDiameter = getWidhtScreen(context) * 0.8;

    size = Screen(MediaQuery.of(context).size);
    return Consumer<LoginChangeNotifier>(
      builder: (context, login, _) {
        return Scaffold(
          backgroundColor: Color(0xFFEEEEEE), //Colors.black,
          key: login.scaffoldKey,
          body: Stack(
            children: [
              // Layer 1: Small Circle on Top Right
              Positioned(
                right: -smallDiameter * 0.2,
                top: -smallDiameter * 0.5,
                child: Container(
                  width: smallDiameter,
                  height: smallDiameter,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.amber[300], Colors.amber[100]]),
                  ),
                ),
              ),

              // Layer 2: Big Circle on Top Left
              Positioned(
                left: -bigDiameter * 0.2,
                top: -bigDiameter * 0.25,
                child: Container(
                  width: bigDiameter,
                  height: bigDiameter,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Colors.amber[500], Colors.amber[100]],
                    ),
                  ),
                ),
              ),

              // Layer 3: Text :: Welcome to Ad1Parter
              Positioned(
                left: 20,
                top: 80,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome to",
                        style: TextStyle(fontSize: 20, color: Colors.grey[700]),
                      ),
                      Text(
                        "Ad1Partner",
                        style: TextStyle(fontSize: 30, color: Colors.grey[700]),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(30, 3, 0, 0),
                        child: Text(
                          "Supported by Adira Finance",
                          style:
                              TextStyle(fontSize: 12, color: Colors.grey[700]),
                        ),
                      ),
                    ],
                  ),
                ),
              ),

              // Layer 4: Small Circle on Bottom Left
              Positioned(
                right: -smallDiameter * 0.3,
                bottom: -smallDiameter * 0.5,
                child: Container(
                  width: smallDiameter,
                  height: smallDiameter,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      gradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [Colors.grey[50], Colors.amber[200]])),
                ),
              ),

              // Layer 5: Text ::  Copyright
              Positioned(
                right: 25,
                bottom: 20,
                child: Container(
                  child: Text(
                    "Copyright @ 2021",
                    style: TextStyle(color: Colors.grey[700], fontSize: 11),
                  ),
                ),
              ),

              Align(
                alignment: Alignment.bottomCenter,
                child: ListView(
                  children: [
                    // Container: Login Card
                    // buildContainerLoginCard(context),
                    Container(
                      margin: EdgeInsets.fromLTRB(10, 250, 10, 0),
                      padding: EdgeInsets.all(10),
                      decoration:
                          BoxDecoration(borderRadius: BorderRadius.circular(4)),
                      child: Card(
                        elevation: 8,
                        child: Container(
                          padding: EdgeInsets.fromLTRB(15, 20, 20, 15),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            color: kPrimaryColor, //Colors.amberAccent,
                          ),
                          child: Column(
                            children: [
                              // Column 1: Textfile Username
                              TextField(
                                controller: login.controllerEmail,
                                decoration: InputDecoration(
                                  icon: Icon(
                                    Icons.person,
                                    color: Colors.grey[700],
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.grey[700])),
                                  labelText: "Username",
                                  labelStyle: TextStyle(color: Colors.grey),
                                ),
                                keyboardType: TextInputType.emailAddress,
                              ),

                              // Column 2: Textfield Password
                              TextField(
                                controller: login.controllerPassword,
                                obscureText: true,
                                decoration: InputDecoration(
                                    icon: Icon(
                                      Icons.vpn_key,
                                      color: Colors.grey[700],
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.grey[700]),
                                    ),
                                    labelText: "Password",
                                    labelStyle: TextStyle(color: Colors.grey)),
                              ),
                              Container(
                                margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                                child: Row(
                                  children: [
                                    // Row 1: Login Button
                                    login.loginProcess
                                        ? Center(
                                            child: CircularProgressIndicator(
                                            backgroundColor: Colors.black,
                                          ))
                                        : RaisedButton(
                                            elevation: 5,
                                            child: Text(
                                              "Sign In",
                                              style: TextStyle(
                                                  color: Colors.amber[100]),
                                            ),
                                            color: Colors.grey[600],
                                            shape: StadiumBorder(),
                                            onPressed: () {
                                              login.check(context);
                                              // Navigator.pushReplacement(
                                              //     context,
                                              //     MaterialPageRoute(
                                              //         builder: (context) =>
                                              //             HomePage()));
                                            },
                                          ),
                                    Spacer(flex: 3),
                                    // Row 2: Forgot Password
                                    Text(
                                      "Forgot Password?",
                                      style: TextStyle(color: Colors.grey[700]),
                                      textAlign: TextAlign.right,
                                    ),
                                    Spacer(flex: 1)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // Container: Social Media
                    buildContainerSocialMedia(),
                    // Container: Don't have an account
                    buildContainerSignUp()
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

// Container: Login Card
  // Container buildContainerLoginCard(BuildContext context) {
  //   return Container(
  //     margin: EdgeInsets.fromLTRB(10, 250, 10, 0),
  //     padding: EdgeInsets.all(10),
  //     decoration: BoxDecoration(borderRadius: BorderRadius.circular(4)),
  //     child: Card(
  //       elevation: 8,
  //       child: Container(
  //         padding: EdgeInsets.fromLTRB(15, 20, 20, 15),
  //         decoration: BoxDecoration(
  //           borderRadius: BorderRadius.circular(4),
  //           color: kPrimaryColor, //Colors.amberAccent,
  //         ),
  //         child: Column(
  //           children: [
  //             // Column 1: Textfile Username
  //             TextField(
  //               controller: login,
  //               decoration: InputDecoration(
  //                 icon: Icon(
  //                   Icons.person,
  //                   color: Colors.grey[700],
  //                 ),
  //                 focusedBorder: UnderlineInputBorder(
  //                     borderSide: BorderSide(color: Colors.grey[700])),
  //                 labelText: "Username",
  //                 labelStyle: TextStyle(color: Colors.grey),
  //               ),
  //             ),

  //             // Column 2: Textfield Password
  //             TextField(
  //               controller: _tecPassword,
  //               obscureText: true,
  //               decoration: InputDecoration(
  //                   icon: Icon(
  //                     Icons.vpn_key,
  //                     color: Colors.grey[700],
  //                   ),
  //                   focusedBorder: UnderlineInputBorder(
  //                     borderSide: BorderSide(color: Colors.grey[700]),
  //                   ),
  //                   labelText: "Password",
  //                   labelStyle: TextStyle(color: Colors.grey)),
  //             ),
  //             Container(
  //               margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
  //               child: Row(
  //                 children: [
  //                   // Row 1: Login Button
  //                   _loginProcess
  //                       ? Center(
  //                           child: CircularProgressIndicator(
  //                           backgroundColor: Colors.black,
  //                         ))
  //                       : RaisedButton(
  //                           elevation: 5,
  //                           child: Text(
  //                             "Sign In",
  //                             style: TextStyle(color: Colors.amber[100]),
  //                           ),
  //                           color: Colors.grey[600],
  //                           shape: StadiumBorder(),
  //                           onPressed: () {
  //                             // _loginTest();
  //                             Navigator.pushReplacement(
  //                                 context,
  //                                 MaterialPageRoute(
  //                                     builder: (context) => HomePage()));
  //                           },
  //                         ),
  //                   Spacer(flex: 3),
  //                   // Row 2: Forgot Password
  //                   Text(
  //                     "Forgot Password?",
  //                     style: TextStyle(color: Colors.grey[700]),
  //                     textAlign: TextAlign.right,
  //                   ),
  //                   Spacer(flex: 1)
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  // Container: Social Media
  Container buildContainerSocialMedia() {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 5, 25, 0),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: 30,
            height: 30,
            child: FloatingActionButton(
                elevation: 2,
                mini: true,
                heroTag: null,
                backgroundColor: kPrimaryColor, //Colors.amberAccent,
                child: Image(
                  image: AssetImage("assets/images/instagram.png"),
                  color: Colors.grey[700],
                ),
                onPressed: () {}),
          ),
          Spacer(),
          SizedBox(
            height: 30,
            child: Center(
              child: Text(
                "@adirafinanceid",
                style: TextStyle(color: Colors.grey[700]),
              ),
            ),
          ),
          Spacer(flex: 6),
          SizedBox(
            width: 30,
            height: 30,
            child: FloatingActionButton(
                elevation: 2,
                mini: true,
                heroTag: null,
                backgroundColor: Colors.amberAccent,
                child: Image(
                  image: AssetImage("assets/images/twitter.png"),
                  color: Colors.grey[700],
                ),
                onPressed: () {}),
          ),
          Spacer(),
          SizedBox(
            height: 30,
            child: Center(
              child: Text(
                "@adirafinanceid",
                style: TextStyle(color: Colors.grey[700]),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Container: Don't have an account
  Container buildContainerSignUp() {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 20, 25, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Don't have an account?  ",
            style: TextStyle(color: Colors.grey[700]),
          ),
          Text(
            "SIGN UP",
            style: TextStyle(color: Colors.amber[600]),
          ),
          // Navigator.of(context)
          //             .push(MaterialPageRoute(builder: (context) => SignUp()));
        ],
      ),
    );
  }
}
