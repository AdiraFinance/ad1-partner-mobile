import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_domisili.dart';

class ExamplePageState extends StatefulWidget {
  final ValueChanged<Map> onSelected;

  const ExamplePageState({Key key, this.onSelected}) : super(key: key);

  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePageState> {
  DataDomisili _dataDomisili;
  List tempList = new List();
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio();
  String _searchText = "";
  List names = new List();
  List filteredNames = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Search Kelurahan');

  _ExamplePageState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredNames = names;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    this._getNames();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _buildList(),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      backgroundColor: Colors.white,
      toolbarHeight: 100,
      centerTitle: true,
      title: new TextFormField(
        autofocus: true,
        keyboardType: TextInputType.text,
        controller: _filter,
        onChanged: (value) {
          if (_filter.text.length > 4) {
            _getNames();
          }
        },
        decoration: InputDecoration(
            labelText: '',
            labelStyle: TextStyle(color: Colors.black),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.blue)),
            suffixIcon: IconButton(
              icon: const Icon(
                Icons.cancel,
                color: Colors.grey,
              ),
              onPressed: () async {
                _filter.clear();
              },
            ),
            prefixIcon: IconButton(
              icon: const Icon(
                Icons.search_rounded,
                color: Colors.grey,
              ),
              onPressed: () async {},
            )),
      ),
    );
  }

  Widget _buildList() {
    if (!(_searchText.isEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredNames.length; i++) {
        if (filteredNames[i]['PARA_KELURAHAN_DESC']
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          tempList.add(filteredNames[i]);
        }
      }
      filteredNames = tempList;
    }
    if (tempList.length == 0 && !(_searchText.isEmpty)) {
      return Scaffold(
        body: SingleChildScrollView(
          child: Container(
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 50),
                  Image.asset(
                    'assets/images/empty@2x.png',
                    height: 200,
                    width: 200,
                  ),
                  Text(
                    "Maaf pencarian tidak ditemukan",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 20),
                  Text(
                      "Kelurahan " +
                          '"' +
                          _searchText +
                          '"' +
                          " tidak ditemukan. Silahkan tulis manual.",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 14)),
                  SizedBox(height: 200),
                  ButtonTheme(
                    minWidth: 100,
                    height: 50,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                          side: BorderSide(color: Colors.blue)),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: Colors.white,
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Kembali",
                            style: TextStyle(color: Colors.blue, fontSize: 20),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              )),
        ),
      );
    } else if (tempList.isNotEmpty) {
      return ListView.builder(
        itemCount: names == null ? 0 : filteredNames.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 4),
              child: new ListTile(
                title: Text("${filteredNames[index]['PARA_KECAMATAN_DESC'].trim()}" +
                    ', Kec.' +
                    "${filteredNames[index]['PARA_KELURAHAN_DESC'].trim()}" +
                    ', ' +
                    "${filteredNames[index]['KAB_KOT_DESC'].trim()}" +
                    ', ' +
                    "${filteredNames[index]['PARA_KELURAHAN_ZIP_CODE'].trim()}"),
                onTap: () {
                  widget.onSelected(filteredNames[index]);
                  Navigator.pop(context);
                },
              ),
            ),
          );
        },
      );
    }
  }

  void _getNames() async {
    final response = await dio.get(
        "https://af-extwsuat.adira.co.id/Ad1gateAPI/api/Submit/GetKecamatan?search=" +
            _filter.text);
    tempList = new List();
    for (int i = 0; i < response.data['Data'].length; i++) {
      tempList.add(response.data['Data'][i]);
    }
    setState(() {
      names = tempList;
      names.shuffle();
      filteredNames = names;
    });
  }
}
