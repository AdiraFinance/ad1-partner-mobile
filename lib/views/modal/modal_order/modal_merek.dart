import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
// import 'package:adira_one/pembiayaan.dart';
// import 'package:work_ui/pembiayaandurable.dart';
// import 'package:work_ui/pembiayaanmultiguna.dart';

class mMerekState extends StatefulWidget {
  @override
  _mMerekState createState() => _mMerekState();
}

class _mMerekState extends State<mMerekState> {
  // Pembiayaan _pembiayaan;
  // PembiayaanDurable _pembiayaanDurable;
  // PembiayaanMultiGuna _pembiayaanMultiGuna;
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio();
  String _searchText = "";
  List names = new List();
  List filteredNames = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = new Text('Search Merek');

  _mMerekState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredNames = names;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getMereks();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Container(
        child: _buildList(),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      backgroundColor: Colors.white,
      toolbarHeight: 100,
      centerTitle: true,
      title: new TextFormField(
        keyboardType: TextInputType.text,
        controller: _filter,
        onChanged: (value) {
          if (_filter.text.length > 3) {
            _getMereks();
          }
        },
        decoration: InputDecoration(
            labelText: '',
            labelStyle: TextStyle(color: Colors.black),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.blue)),
            suffixIcon: IconButton(
              icon: const Icon(
                Icons.cancel,
                color: Colors.grey,
              ),
              onPressed: () async {
                _filter.clear();
              },
            ),
            prefixIcon: IconButton(
              icon: const Icon(
                Icons.search_rounded,
                color: Colors.grey,
              ),
              onPressed: () async {},
            )),
      ),
    );
  }

  Widget _buildList() {
    if ((_searchText.isNotEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredNames.length; i++) {
        if (filteredNames[i]['OBMO_DESC']
                .toLowerCase()
                .contains(_searchText.toLowerCase()) ||
            filteredNames[i]['OBBR_DESC']
                .toLowerCase()
                .contains(_searchText.toLowerCase()) ||
            filteredNames[i]['OBTY_DESC']
                .toLowerCase()
                .contains(_searchText.toLowerCase())) {
          tempList.add(filteredNames[i]);
        }
      }
      filteredNames = tempList;
    }
    return ListView.builder(
      itemCount: names == null ? 0 : filteredNames.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 3),
            child: new ListTile(
              title: Text("${filteredNames[index]['OBMO_DESC'].trim()}" +
                  ', ' +
                  "${filteredNames[index]['OBBR_DESC'].trim()}" +
                  ', ' +
                  "${filteredNames[index]['OBTY_DESC'].trim()}"),
              onTap: () => Navigator.pop(context),
            ),
          ),
        );
      },
    );
  }

  void _getMereks() async {
    final response = await dio.post(
        'https://af-extwsuat.adira.co.id/Ad1gateAPIMobileUAT/api/Submit/GetModel',
        data: {
          'search': 'a',
          'branchid': '0321',
          'groupcode': '002',
          'dlc': '049065'
        });
    List templist = new List();
    for (int i = 0; i < response.data['Data'].length; i++) {
      templist.add(response.data['Data'][i]);
    }
    setState(() {
      names = templist;
      names.shuffle();
      filteredNames = names;
    });
  }
}
