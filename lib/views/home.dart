import 'package:adira_one_partner/services/home_change_notifier.dart';
import 'package:adira_one_partner/services/login_change_notifier.dart';
import 'package:adira_one_partner/views/login/login.dart';
import 'package:adira_one_partner/views/submit_order/durable/submit_order_durable.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/submit_order.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../utils/constanta.dart';
import '../utils/size_config.dart';
// import 'package:adira_one_partner/db_helper/database_helper.dart';
// import 'service/login_api_provider.dart';

class HomePage extends StatefulWidget {
  final int dif;
  final int expiredToken;

  const HomePage({Key key, this.dif, this.expiredToken}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Screen size;
  // LoginApiProvider _loginApiProvider = LoginApiProvider();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  String portfolio;

  @override
  void initState() {
    super.initState();
    _getMatrixPartner();
    _checkPortofolio();
  }

  _getMatrixPartner() async {
    Provider.of<HomeChangeNotifier>(context, listen: false)
        .getMatrixPartner(context);
    Provider.of<HomeChangeNotifier>(context, listen: false)
        .getInfoPartner(context);
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
        key:
            Provider.of<HomeChangeNotifier>(context, listen: false).scaffoldKey,
        backgroundColor: Colors.white,
        // appBar: AppBar(
        //     backgroundColor: Colors.white,
        //     title: Row(
        //       children: [
        //         Image.asset('assets/images/logo_adira.png', scale: 2),
        //         SizedBox(
        //           width: size.wp(27),
        //         ),
        //         Align(
        //           // margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
        //           alignment: Alignment.centerRight,
        //           child: Text("Selamat Pagi, Joni!",
        //               textAlign: TextAlign.end,
        //               style: TextStyle(
        //                 fontFamily: "Nunito Sans",
        //                 fontSize: 14,
        //               )),
        //         )
        //       ],
        //     ),
        //     centerTitle: false,
        //     automaticallyImplyLeading: false,
        //     actions: <Widget>[
        //       IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
        //     ]),
        body: Container(
            margin: EdgeInsets.all(10),
            // width: 380,
            // height: 400,
            color: Colors.white,
            alignment: Alignment.center,
            child:
                Consumer<HomeChangeNotifier>(builder: (context, data, child) {
              return data.loadData
                  ? Center(
                      child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(height: size.hp(4)),
                        Text("Loading Get Info")
                      ],
                    ))
                  // : Container();
                  : Stack(
                      fit: StackFit.passthrough,
                      // overflow: Overflow.visible,
                      clipBehavior: Clip.none, //replace for overflow
                      children: [
                        ListView(
                            // physics: const NeverScrollableScrollPhysics(),
                            padding: EdgeInsets.only(
                                top: 16, bottom: 16, left: 5, right: 0),
                            scrollDirection: Axis.vertical,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Container(
                                    width: 150,
                                    height: 80,
                                    margin: EdgeInsets.only(right: 5, left: 10),
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        // fit: BoxFit.contain,
                                        alignment: Alignment.centerLeft,
                                        // scale: 2,
                                        image: AssetImage(
                                            "assets/images/logo_adira.png"),
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                      alignment: Alignment.centerRight,
                                      icon:
                                          Image.asset("assets/images/bell.png"),
                                      onPressed: () {
                                        _logout();
                                      })
                                ],
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 5, left: 10),
                                child: Text(
                                  "Hai",
                                  // "${data.dealerMatrix[0].fullName}" != "null"
                                  //     ? "Hai, ${data.dealerMatrix[0].fullName}"
                                  //     : "Hai, ",
                                  style: TextStyle(
                                    color: Color(0xff12162c),
                                    fontSize: 16,
                                    fontFamily: "Nunito Sans",
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    // left: 8,
                                    // top: 12,
                                    margin: EdgeInsets.only(right: 5, left: 10),
                                    child: SizedBox(
                                      // width: 191,
                                      height: 16,
                                      child: Text(
                                        "Honda",
                                        style: TextStyle(
                                          color: Color(0xff12162c),
                                          fontSize: 14,
                                          fontFamily: "Nunito Sans",
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                      child: Icon(
                                    Icons.circle,
                                    size: 10,
                                  )),
                                  Container(
                                    // left: 0,
                                    // top: 12,
                                    margin: EdgeInsets.only(left: 5),
                                    child: SizedBox(
                                      // width: 191,
                                      height: 16,
                                      child: Text(
                                        // "${data.dealerInfo[0].dealName}",
                                        "Mitra Cendekia Motor",
                                        style: TextStyle(
                                          color: Color(0xff12162c),
                                          fontSize: 14,
                                          fontFamily: "Nunito Sans",
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: size.hp(2)),
                              Positioned.fill(
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    width: size.wp(95),
                                    height: size.hp(20),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Color(0xfffcebb1),
                                    ),
                                    padding: const EdgeInsets.all(12),
                                    margin: EdgeInsets.only(left: 5, right: 10),
                                    child: Column(
                                      // mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          // width: 340,
                                          // height: 20,
                                          margin: EdgeInsets.only(top: 10),
                                          child: Row(
                                            children: [
                                              Text(
                                                "Transaksi April 2021",
                                                style: TextStyle(
                                                  color: Color(0xff12162c),
                                                  fontSize: 14,
                                                  fontFamily: "Nunito Sans",
                                                  fontWeight: FontWeight.w700,
                                                ),
                                                textAlign: TextAlign.left,
                                              ),
                                            ],
                                          ),
                                          // color: Colors.blue,
                                        ),
                                        // SizedBox(height: 5),
                                        Container(
                                          width: 340,
                                          height: 80,
                                          // margin: EdgeInsets.fromLTRB(15, 5, 15, 5),
                                          alignment: Alignment.center,
                                          // padding: EdgeInsets.fromLTRB(5, 0, 8, 0),
                                          // color: Colors.blue,
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                width: 320,
                                                height: 80,
                                                // color: Colors.amber,
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  boxShadow: [
                                                    BoxShadow(
                                                      color: Color(0x0c000000),
                                                      blurRadius: 4,
                                                      offset: Offset(0, 4),
                                                    ),
                                                  ],
                                                  color: Colors.white,
                                                ),
                                                padding:
                                                    const EdgeInsets.all(18),
                                                child: Row(
                                                  // mainAxisSize: MainAxisSize.max,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          // width: 80, //100
                                                          // height: 10, //18
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          child: Text(
                                                            "ORDER SUKSES",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff3d485c),
                                                              fontSize: 11,
                                                              letterSpacing:
                                                                  0.55,
                                                            ),
                                                            textAlign:
                                                                TextAlign.left,
                                                          ),
                                                        ),
                                                        Container(
                                                          // width: 26,
                                                          // height: 24,
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          child: Text(
                                                            "14",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff12162c),
                                                              fontSize: 16,
                                                              fontFamily:
                                                                  "Poppins",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                            ),
                                                            textAlign:
                                                                TextAlign.left,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          // width: 87,
                                                          // height: 16,
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          child: Text(
                                                            "PERFORMA",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff3d485c),
                                                              fontSize: 11,
                                                              letterSpacing:
                                                                  0.55,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          // width: 44,
                                                          // height: 24,
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 5),
                                                          child: Text(
                                                            "Baik",
                                                            // "${data.dealerMatrix[0].gradeNmcy}" !=
                                                            //         "null"
                                                            //     ? "${data.dealerMatrix[0].gradeNmcy}"
                                                            //     : "Baik",
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff12162c),
                                                              fontSize: 16,
                                                              fontFamily:
                                                                  "Poppins",
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    IconButton(
                                                      alignment:
                                                          Alignment.centerRight,
                                                      icon: Icon(
                                                          Icons.arrow_forward,
                                                          color: Colors.blue),
                                                      onPressed: () {},
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: size.hp(2)),
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Text(
                                  'Order Berlangsung',
                                  style: TextStyle(
                                      fontFamily: "Poppins",
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              SizedBox(height: size.hp(2)),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(
                                    // height: 40,
                                    // width: 100,
                                    margin: EdgeInsets.only(left: 5),
                                    child: TextButton(
                                      onPressed: () {},
                                      child: Row(
                                        children: [
                                          Image(
                                              image: AssetImage(
                                                  "assets/images/filter.png")),
                                          SizedBox(width: size.wp(2)),
                                          Text("Filter",
                                              style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.w400,
                                              ))
                                        ],
                                      ),
                                      style: ButtonStyle(
                                          padding: MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.fromLTRB(
                                                  16, 4, 16, 4)),
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.white),
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Color(0xFF4F5262)),
                                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(16.0),
                                                  side: BorderSide(color: Colors.grey)))),
                                    ),
                                  ),
                                  Container(
                                    // height: 32,
                                    margin: EdgeInsets.only(left: 5),
                                    child: TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        "Berlangsung",
                                        style: TextStyle(fontSize: 14),
                                      ),
                                      style: ButtonStyle(
                                          padding: MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.fromLTRB(
                                                  16, 4, 16, 4)),
                                          backgroundColor: MaterialStateProperty.all<Color>(
                                              Color(0xFFEFFFF8)),
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.black),
                                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(16.0),
                                                  side: BorderSide(color: Color(0xFF28C27D))))),
                                    ),
                                  ),
                                  Container(
                                    // height: 40,
                                    // width: 100,
                                    margin: EdgeInsets.only(left: 5),
                                    child: TextButton(
                                      onPressed: () {},
                                      child: Text("Draft",
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                          )),
                                      style: ButtonStyle(
                                          padding: MaterialStateProperty.all<EdgeInsets>(
                                              EdgeInsets.fromLTRB(
                                                  16, 4, 16, 4)),
                                          backgroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Colors.white),
                                          foregroundColor:
                                              MaterialStateProperty.all<Color>(
                                                  Color(0xFF4F5262)),
                                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(16.0),
                                                  side: BorderSide(color: Colors.grey)))),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: size.hp(2)),
                              Container(
                                // height: 300,
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
                                color: Color(0xFFF6F6F6),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text("01 April 2021",
                                          textAlign: TextAlign.left),
                                    ),
                                    Container(
                                      height: 70,
                                      child: Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          elevation: 5,
                                          margin: EdgeInsets.all(2),
                                          child: Container(
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                  child: Image.asset(
                                                      "assets/images/BS.png"),
                                                  flex: 2,
                                                ),
                                                Expanded(
                                                  flex: 5,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Text(
                                                        "Jaya Suparno",
                                                        style: TextStyle(
                                                            fontSize: 14),
                                                      ),
                                                      Text(
                                                          "Honda Astrea Legenda",
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              color: Color(
                                                                  0xff9C9EA9)))
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 3,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Align(
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: IconButton(
                                                          // padding: EdgeInsets.fromLTRB(
                                                          //     80, 0, 0, 0),
                                                          icon: Image.asset(
                                                              'assets/images/vector.png'),
                                                          // iconSize: 20,
                                                          onPressed: () {},
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Container(
                                                          // margin:
                                                          //     EdgeInsets.only(right: 10),
                                                          alignment: Alignment
                                                              .topCenter,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          4),
                                                              color: Color(
                                                                  0xF2F7FF)),
                                                          child: Text(
                                                            "PROSES VERIFIKASI",
                                                            style: TextStyle(
                                                                fontSize: 8,
                                                                color: Color(
                                                                    0xff6789C6)),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          )),
                                    ),
                                    Container(
                                      height: 70,
                                      child: Card(
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                          elevation: 5,
                                          margin: EdgeInsets.all(2),
                                          child: Container(
                                            child: Row(
                                              children: <Widget>[
                                                Expanded(
                                                  child: Image.asset(
                                                      "assets/images/BS.png"),
                                                  flex: 2,
                                                ),
                                                Expanded(
                                                  flex: 5,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Text(
                                                        "Jaya Suparno",
                                                        style: TextStyle(
                                                            fontSize: 14),
                                                      ),
                                                      Text(
                                                          "Honda Astrea Legenda",
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              color: Color(
                                                                  0xff9C9EA9)))
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                  flex: 3,
                                                  child: Column(
                                                    mainAxisSize:
                                                        MainAxisSize.min,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Align(
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: IconButton(
                                                          // padding: EdgeInsets.fromLTRB(
                                                          //     80, 0, 0, 0),
                                                          icon: Image.asset(
                                                              'assets/images/vector.png'),
                                                          // iconSize: 20,
                                                          onPressed: () {},
                                                        ),
                                                      ),
                                                      Align(
                                                        alignment:
                                                            Alignment.topRight,
                                                        child: Container(
                                                          // margin:
                                                          //     EdgeInsets.only(right: 10),
                                                          alignment: Alignment
                                                              .topCenter,
                                                          decoration: BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          4),
                                                              color: Color(
                                                                  0xF2F7FF)),
                                                          child: Text(
                                                            "PROSES VERIFIKASI",
                                                            style: TextStyle(
                                                                fontSize: 8,
                                                                color: Color(
                                                                    0xff6789C6)),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          )),
                                    ),
                                  ],
                                ),
                              ),
                            ]),
                        Positioned(
                          // width: 200,
                          // height: 50,
                          right: size.wp(8),
                          top: size.hp(14),
                          child: Image.asset("assets/images/person.png"),
                        )
                      ],
                    );
            })),
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: 0,
            iconSize: 30.0,
            // backgroundColor: Colors.amber,
            unselectedItemColor: Colors.black,
            selectedItemColor: Colors.amber,
            type: BottomNavigationBarType.fixed,
            items: [
              BottomNavigationBarItem(
                  title: Text(
                    'Beranda',
                    style: TextStyle(color: Colors.black),
                  ),
                  icon: Image(image: AssetImage("assets/images/home.png"))),
              BottomNavigationBarItem(
                  title: Text('Lacak Order'),
                  icon:
                      Image(image: AssetImage("assets/images/search-alt.png"))),
              BottomNavigationBarItem(
                  title: Text('Simulasi'),
                  icon:
                      Image(image: AssetImage("assets/images/calculator.png"))),
              BottomNavigationBarItem(
                  title: Text('Profil'),
                  icon: Image(image: AssetImage("assets/images/user.png")))
            ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActionButton.extended(
          backgroundColor: HexColor("#FFDD00"),
          foregroundColor: Colors.black,
          onPressed: () {
            // Respond to button press
            _submitOrder();
          },
          icon: Icon(Icons.add),
          label: Text(
            'Order Baru',
            style: TextStyle(
                fontSize: 16,
                fontFamily: "Nunito Sans",
                fontWeight: FontWeight.bold),
          ),
        ));
  }

  _submitOrder() {
    print(portfolio);
    if (portfolio == 'Nmcy') {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => DataKonsumen()));
      // Navigator.push(
      //     context, MaterialPageRoute(builder: (context) => DataKonsumen()));
    } else if (portfolio == 'Umcy') {
      //
    } else if (portfolio == 'Ncar') {
      //
    } else if (portfolio == 'Ucar') {
      //
    } else if (portfolio == 'Durable') {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => DataKonsumenDurable()));
    } else if (portfolio == 'Jasa') {
      //
    } else {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => DataKonsumen()));
    }
  }

  _checkPortofolio() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    print('cek portfolio');
    print(_preference.getString("portfolio"));
    setState(() {
      portfolio = _preference.getString("portfolio");
    });
  }

  _logout() async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    await _preference.clear();
    var _providerLogin =
        Provider.of<LoginChangeNotifier>(context, listen: false);
    _providerLogin.controllerPassword.clear();
    _providerLogin.autoValidate = false;
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginPage()),
        (Route<dynamic> route) => false);
  }
  // _logout() async {
  //   try {
  //     print("testLogout");
  //     var _result = await _loginApiProvider.logout();
  //     if (_result == "200") {
  //       var _getAllData = await _dbHelper.getToken();
  //       var _result = await _dbHelper.updateTokenLogout(_getAllData[0]['id']);
  //       if (_result == 1) {
  //         Navigator.pushAndRemoveUntil(
  //             context,
  //             PageRouteBuilder(pageBuilder: (BuildContext context,
  //                 Animation animation, Animation secondaryAnimation) {
  //               return LoginPage();
  //             }, transitionsBuilder: (BuildContext context,
  //                 Animation<double> animation,
  //                 Animation<double> secondaryAnimation,
  //                 Widget child) {
  //               return new SlideTransition(
  //                 position: new Tween<Offset>(
  //                   begin: const Offset(1.0, 0.0),
  //                   end: Offset.zero,
  //                 ).animate(animation),
  //                 child: child,
  //               );
  //             }),
  //             (Route route) => false);
  //       }
  //     }
  //   } catch (e) {
  //     _showSnackBar(e);
  //   }
  // }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(text), behavior: SnackBarBehavior.floating));
  }

  // _showDialogWarningTimeToken(int minute) {
  //   showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           title: Text("Warning", style: TextStyle(fontFamily: "NunitoSans")),
  //           content: Text(
  //               "Sesi anda akan berakhir $minute menit lagi,\nSilahkan logout dan login kembali"),
  //           actions: <Widget>[
  //             FlatButton(
  //                 onPressed: () {
  //                   Navigator.pop(context);
  //                 },
  //                 child: Text("CLOSE",
  //                     style: TextStyle(
  //                         fontFamily: "NunitoSans", color: kPrimaryColor))),
  //           ],
  //         );
  //       });
  // }
}
