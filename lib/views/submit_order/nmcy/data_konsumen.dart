import 'dart:io';

import 'package:adira_one_partner/components/link_btn.dart';
import 'package:adira_one_partner/components/tf.dart';
import 'package:adira_one_partner/services/submit_data_konsumen_notifier.dart';
import 'package:adira_one_partner/utils/size_config.dart';
import 'package:adira_one_partner/widgets/foto_camera.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class InputDataNasabah extends StatefulWidget {
  @override
  InputDataNasabahState createState() => InputDataNasabahState();
}

class InputDataNasabahState extends State<InputDataNasabah> {
  Screen size;

  var nikController = TextEditingController();
  var fullNameController = TextEditingController();
  var phoneNoController = TextEditingController();
  var waNoController = TextEditingController();
  int _radioCustTypeValue = 0;
  String noIdCaption = 'NIK';
  String namaCaption = 'Nama Lengkap Sesuai KTP';

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Consumer<SubmitDataConsumen>(
        builder: (_, data, __) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Data Konsumen",
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 18,
                      fontFamily: "Poppins"),
                ),
                Divider(
                  color: Colors.black,
                  height: 36,
                ),
                Text(
                  "Tipe Konsumen",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 12,
                      fontFamily: "Nunito Sans"),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Radio(
                        activeColor: Colors.green,
                        value: 0,
                        groupValue: _radioCustTypeValue,
                        onChanged: _handleRadioCustTypeValueChange),
                    Text(
                      "Individu",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Nunito Sans"),
                    ),
                    Radio(
                        activeColor: Colors.green,
                        value: 1,
                        groupValue: _radioCustTypeValue,
                        onChanged: _handleRadioCustTypeValueChange),
                    Text(
                      "Perusahaan",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Nunito Sans"),
                    ),
                  ],
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                Text(
                  _radioCustTypeValue == 0 ? "KTP Konsumen" : "NPWP Konsumen",
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 12,
                      fontFamily: "Nunito Sans"),
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey[300],
                        width: 1,
                      ),
                      borderRadius: BorderRadius.circular(8)),
                  padding: EdgeInsets.all(2),
                  child: ListTile(
                    leading: data.imageFile == null
                        ? Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Icon(Icons.camera_enhance),
                          )
                        : Container(
                            width: 70,
                            height: 70,
                            child:
                                Image.file(data.imageFile, fit: BoxFit.cover),
                          ),
                    title: Align(
                      alignment: Alignment.centerLeft,
                      child: LinkBtn(
                          onPress: () => showDialog(
                                context: context,
                                builder: (BuildContext ctx) =>
                                    _dialogPopUpCamera(ctx),
                              ),
                          text: data.imageFile == null
                              ? _radioCustTypeValue == 0
                                  ? 'Unggah Foto KTP'
                                  : 'Unggah Foto NPWP'
                              : _radioCustTypeValue == 0
                                  ? 'KTP Sukses diunggah'
                                  : 'NPWP Sukses diunggah'),
                    ),
                    subtitle: Container(
                        padding: EdgeInsets.only(left: 25),
                        child: data.imageFile == null
                            ? _radioCustTypeValue == 0
                                ? Text('Silahkan unggah ktp konsumen')
                                : Text('Silahkan unggah npwp konsumen')
                            : Text('Ketuk untuk lihat')),
                    trailing: data.imageFile != null
                        ? IconButton(
                            color: Theme.of(context).errorColor,
                            icon: Icon(Icons.delete),
                            onPressed: () => Provider.of<SubmitDataConsumen>(
                                    context,
                                    listen: false)
                                .dispose(),
                          )
                        : null,
                  ),
                ),
                SizedBox(
                  height: size.hp(3),
                ),
                Text(
                  noIdCaption,
                  style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Nunito Sans",
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                AppTextFormField(
                  controller: data.nikController,
                  onSaved: (newValue) {
                    data.setNikController = newValue;
                  },
                  hintText: _radioCustTypeValue == 0
                      ? 'Tulis Nomor NIK'
                      : 'Tulis Nomor NPWP',
                  enabled: true,
                  keyboardType: TextInputType.number,
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                Text(
                  namaCaption,
                  style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Nunito Sans",
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                AppTextFormField(
                  controller: data.fullNameController,
                  onSaved: (newValue) {
                    data.setFullNameController = newValue;
                  },
                  hintText: 'Tulis nama lengkap',
                  enabled: true, //data.getDataOcr == false ? true : false,
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                Text(
                  "Nomor HP yang bisa dihubungi",
                  style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Nunito Sans",
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                AppTextFormField(
                  controller: data.phoneNoController,
                  hintText: 'mis. 08098999999',
                  keyboardType: TextInputType.phone,
                ),
                // SizedBox(
                //   height: size.hp(1),
                // ),
                Row(
                  children: [
                    Checkbox(
                      activeColor: Colors.green,
                      value: data.checkBox,
                      onChanged: (bool value) {
                        var stringValue;
                        value
                            ? stringValue = data.phoneNoController.text
                            : stringValue = "";
                        data.checkBox = value;
                        data.waNoController.text = stringValue;
                        data.sameWithPhone();
                      },
                    ),
                    Text("Centang jika Nomor HP sama dengan nomor WA",
                        style: TextStyle(
                            fontSize: 12,
                            fontFamily: "Nunito Sans",
                            fontWeight: FontWeight.w600)),
                  ],
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                Text(
                  "Nomor WA (Opsional)",
                  style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Nunito Sans",
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(
                  height: size.hp(1),
                ),
                AppTextFormField(
                  controller: data.waNoController,
                  hintText: 'mis. 08098999999',
                  keyboardType: TextInputType.number,
                  enabled: data.waEnable,
                ),
                // SizedBox(
                //   height: size.hp(2),
                // ),
                // Center(
                //   child: TextButton(
                //     onPressed: () {},
                //     child: Text(
                //       "Simpan ke draft",
                //       style: TextStyle(
                //         fontSize: 15,
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ));
  }

  Future getImage(ImageSource source) async {
    final _selectedImage =
        await ImagePicker().getImage(source: source, imageQuality: 100);
    if (_selectedImage != null) {
      File cropped = await ImageCropper.cropImage(
        sourcePath: _selectedImage.path,
        //cropStyle: CropStyle.rectangle,
        //aspectRatioPresets: [CropAspectRatioPreset.original],
        //aspectRatio: CropAspectRatio(ratioX: 4, ratioY: 3),
        compressQuality: 100,
        maxHeight: 800,
        maxWidth: 1200,
        compressFormat: ImageCompressFormat.jpg,
        androidUiSettings: AndroidUiSettings(toolbarTitle: "Crop Image"),
      );
      Provider.of<SubmitDataConsumen>(context).imageFile = cropped;
      context.read<SubmitDataConsumen>().UploadFile();
    }
  }

  //

  Widget _dialogPopUpCamera(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Choose an Acction',
        style: TextStyle(color: Colors.blue),
      ),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
              icon: Icon(Icons.camera_alt_sharp),
              iconSize: 70,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TakeImageCamera()),
                );
              }),
          IconButton(
              icon: Icon(Icons.folder_open),
              iconSize: 70,
              onPressed: () {
                FutureBuilder(
                  future: getImage(ImageSource.gallery),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    snapshot.connectionState == ConnectionState.done
                        ? Navigator.pop(context)
                        : CircularProgressIndicator();
                  },
                );
              })
        ],
      ),
    );
  }

  void _handleRadioCustTypeValueChange(int value) {
    setState(() {
      _radioCustTypeValue = value;
    });
    if (_radioCustTypeValue == 1) {
      noIdCaption = 'No NPWP';
      namaCaption = 'Nama Lengkap Sesuai NPWP';
    } else {
      noIdCaption = 'NIK';
      namaCaption = 'Nama Lengkap Sesuai KTP';
    }
  }
}
