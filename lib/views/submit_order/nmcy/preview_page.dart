import 'package:adira_one_partner/services/submit_data_konsumen_notifier.dart';
import 'package:adira_one_partner/services/submit_domisili_notifier.dart';
import 'package:adira_one_partner/services/submit_order_api_provider.dart';
import 'package:adira_one_partner/services/submit_unit_notifier.dart';
import 'package:adira_one_partner/utils/constanta.dart';
import 'package:adira_one_partner/utils/size_config.dart';
import 'package:adira_one_partner/views/home.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_domisili.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_unit.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/submit_order.dart';
import 'package:flutter/material.dart';
import 'package:adira_one_partner/services/order_api_provider.dart';
import 'package:adira_one_partner/views/home.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreviewPage extends StatefulWidget {
  @override
  _PreviewPageState createState() => _PreviewPageState();
}

class _PreviewPageState extends State<PreviewPage> {
  Screen size;
  SubmitOrderForm _submitOrderForm;
  var _processSendData = false;
  final formatCurrency =
      new NumberFormat.simpleCurrency(decimalDigits: 0, locale: "id_ID");

  OrderApiProvider _OrderApiProvider;

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Preview Order",
          style: TextStyle(
              fontFamily: "Nunito Sans",
              fontSize: 16,
              fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            dataKonsumen(),
            SizedBox(
              height: size.hp(2),
            ),
            domisiliSurvey(),
            SizedBox(
              height: size.hp(2),
            ),
            unitPembiayaan(),
            SizedBox(
              height: size.hp(2),
            ),
            voucher(),
            SizedBox(
              height: size.hp(2),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ButtonBar(
        children: [
          Container(
            height: 48,
            width: double.maxFinite,
            margin: EdgeInsets.all(10),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0)),
                  primary: kPrimaryColor,
                  onPrimary: Colors.black45),
              onPressed: () {
                _processData();
              },
              child: Text(
                "Kirim Order",
                style: TextStyle(
                    fontFamily: "Nunito Sans",
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Colors.black),
              ),
            ),
          )
        ],
      ),
    );
  }

  Container dataKonsumen() {
    return Container(
        margin: EdgeInsets.fromLTRB(size.wp(5), 0, size.wp(2), 0),
        child: Consumer<SubmitDataConsumen>(
          builder: (_, data, __) => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Data Konsumen",
                    style: TextStyle(
                        fontFamily: "Nunito Sans",
                        fontSize: 16,
                        fontWeight: FontWeight.w700),
                  ),
                  TextButton(
                    child: Text(
                      "Ubah",
                      style: TextStyle(
                          fontFamily: "Nunito Sans",
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff0A66C2)),
                    ),
                    onPressed: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DataKonsumen())),
                  ),
                ],
              ),
              const Divider(
                height: 1,
                thickness: 2,
                endIndent: 15,
              ),
              SizedBox(height: size.hp(2)),
              Container(
                height: size.hp(12),
                padding: EdgeInsets.only(left: size.wp(5)),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(8),
                  // color: Color(0xfffcebb1),
                ),
                child: Row(
                  children: [
                    data.imageFile == null
                        ? Image(image: AssetImage("assets/images/user.png"))
                        : Container(
                            width: 70,
                            height: 70,
                            decoration: BoxDecoration(
                                border:
                                    Border.all(width: 1, color: Colors.grey)),
                            child:
                                Image.file(data.imageFile, fit: BoxFit.cover),
                          ),
                    SizedBox(width: size.wp(5)),
                    Text(
                      "Foto KTP",
                      style: TextStyle(
                          fontFamily: "Nunito Sans",
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff92949D)),
                    ),
                  ],
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "NIK Konsumen",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.nikController.text,
                  // "3434-4559-4547-1009",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Nama Sesuai KTP",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.fullNameController.text,
                  // "Jaya Suparno",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Nomor HP yang bisa dihubungi",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.phoneNoController.text,
                  // "0812-4590-3689",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Nomor WA",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.waNoController.text,
                  // "0812-4590-3689",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
            ],
          ),
        ));
  }

  Container domisiliSurvey() {
    return Container(
        margin: EdgeInsets.fromLTRB(size.wp(5), 0, size.wp(2), 0),
        child: Consumer<SubmitDomisiliNotifier>(
          builder: (_, data, __) => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Domisili & Survey",
                    style: TextStyle(
                        fontFamily: "Nunito Sans",
                        fontSize: 16,
                        fontWeight: FontWeight.w700),
                  ),
                  TextButton(
                    child: Text(
                      "Ubah",
                      style: TextStyle(
                          fontFamily: "Nunito Sans",
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff0A66C2)),
                    ),
                    onPressed: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DataDomisili())),
                  ),
                ],
              ),
              const Divider(
                height: 1,
                thickness: 2,
                endIndent: 15,
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Alamat Lengkap Domisili",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.controllerAlamatDomisili.text,
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Kelurahan",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.checkKelurahan == true
                      ? data.controllerKelurahanAdd.text
                      : data.controllerKelurahanAll.text,
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Jadwal Survey",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.controllerSelectedDate.text +
                      data.controllerJamSurvey.text,
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
            ],
          ),
        ));
  }

  Container unitPembiayaan() {
    return Container(
        margin: EdgeInsets.fromLTRB(size.wp(5), 0, size.wp(2), 0),
        child: Consumer<SubmitUnitNotifier>(
          builder: (_, data, __) => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Unit & Pembiayaan",
                    style: TextStyle(
                        fontFamily: "Nunito Sans",
                        fontSize: 16,
                        fontWeight: FontWeight.w700),
                  ),
                  TextButton(
                    child: Text(
                      "Ubah",
                      style: TextStyle(
                          fontFamily: "Nunito Sans",
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Color(0xff0A66C2)),
                    ),
                    onPressed: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DataKonsumen())),
                  ),
                ],
              ),
              const Divider(
                height: 1,
                thickness: 2,
                endIndent: 15,
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Tipe Unit",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.controllerTipeKendaraanAll.text,
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "OTR",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                padding: EdgeInsets.only(right: 20),
                alignment: Alignment.centerLeft,
                child: Text(
                  data.controllerOtr.text,
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "DP",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(right: 20),
                child: Text(
                  data.controllerDp.text,
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Tenor Dalam Bulan",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  data.controllerTenor.text,
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Cicilan per bulan",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ),
              SizedBox(height: size.hp(1)),
              Container(
                padding: EdgeInsets.only(right: 20),
                alignment: Alignment.centerLeft,
                child: Text(
                  data.controllerCicilan.text,
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff12162C)),
                ),
              ),
              SizedBox(height: size.hp(2)),
              Container(
                height: size.hp(10),
                padding: EdgeInsets.only(left: size.wp(5)),
                margin: EdgeInsets.only(right: size.wp(5)),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(8),
                  color: Color(0xffF6F6F6),
                ),
                child: Row(
                  children: [
                    Image(image: AssetImage("assets/images/notif.png")),
                    SizedBox(width: size.wp(2)),
                    Expanded(
                      child: Text(
                        "Pastikan data sudah benar sebelum mengirim order kredit",
                        style: TextStyle(
                            fontFamily: "Nunito Sans",
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff4F5262)),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  Container voucher() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.fromLTRB(size.wp(5), size.hp(5), size.wp(2), 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Divider(
            height: 1,
            thickness: 4,
            endIndent: 15,
          ),
          SizedBox(height: size.hp(5)),
          Text(
            "Masukkan Kode Voucher (opsional)",
            style: TextStyle(
                fontFamily: "Nunito Sans",
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            height: size.hp(10),
            padding: EdgeInsets.only(left: size.wp(5)),
            margin: EdgeInsets.only(right: size.wp(5)),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(8),
              color: Color(0xffF6F9FE),
            ),
            child: Row(
              children: [
                Image(image: AssetImage("assets/images/ticket.png")),
                SizedBox(width: size.wp(2)),
                Expanded(
                  child: TextField(
                    decoration: new InputDecoration(
                      labelText: 'Ketik Kode Voucher disini',
                      labelStyle: TextStyle(fontFamily: "NunitoSans"),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(8)),
                    ),
                    style: TextStyle(
                        fontFamily: "Nunito Sans",
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff92949D)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _OrderApiProvider = OrderApiProvider();
  }

  _processData() async {
    var OrderNo = await _OrderApiProvider.GetOrderNo();
    print(OrderNo);
    SharedPreferences _preference = await SharedPreferences.getInstance();
    _preference.setString("orderNo", OrderNo);
    var _providerDataCustomer =
        Provider.of<SubmitDataConsumen>(context, listen: false);
    var _providerDataDomisili =
        Provider.of<SubmitDomisiliNotifier>(context, listen: false);
    var _providerDataUnit =
        Provider.of<SubmitUnitNotifier>(context, listen: false);
    setState(() {
      _processSendData = true;
    });
    var data = {
      "credSvyRtRw": "string",
      "credApplFlagSource": "01",
      "credIdNo": _providerDataCustomer.nikController.text,
      "credSex": "01",
      "credName": _providerDataCustomer.fullNameController.text,
      "credCompPicName": "string",
      "credRtRwKtp": "string",
      "credProvinceId": "01",
      "credNpwpDate": 1613727706253,
      "credSidName": "string",
      "credBirthDate": 793186906253,
      "credObmoCode": _providerDataUnit.idModelKendaraan,
      "credFotoStnk": "5",
      "credKabKotaId": "0001",
      "credObjtYear": "0001",
      "credTop": _providerDataUnit.controllerTenor.text,
      "credIdDate": 1613727706253,
      "credVoucherCode": "string",
      "credSvyType": "00005",
      "credTimeSurvey": "19:19",
      "credSurveyAddr": _providerDataDomisili.controllerAlamatDomisili.text,
      "credDealNotes": "NOTES",
      "credTelpNo": _providerDataCustomer.phoneNoController.text,
      "credCustType": "001",
      "credKecamatanId": "00001",
      "credFlagAddr": "1",
      "credFotoKtpNpwp": "5",
      "credOccupation": "001",
      "credKelurahanId": "00001",
      "credMobilePhNo": _providerDataCustomer.waNoController.text,
      "credMktNik": "10991430",
      "credBirthPlace": "infonedia",
      "credInstAmt": 0,
      // _providerDataUnit.controllerCicilan.text
      //     .replaceAll("Rp ", "")
      //     .replaceAll(",", ""),
      "credNetDp": 0,
      // _providerDataUnit.controllerDp.text
      //     .replaceAll("Rp ", "")
      //     .replaceAll(",", ""),
      "credFinProduct": "0",
      "credObjtCode": "001",
      "credTypeBarang": "string",
      "credSvyZipCode": _providerDataDomisili.zipCode,
      "credJenisUsaha": "0001",
      "credObtyCode": _providerDataUnit.idTypeKendaraan,
      "credObjtPrice": _providerDataUnit.controllerOtr.text
          .replaceAll("Rp ", "")
          .replaceAll(",", ""),
      "credKtpAddr": "indonesia",
      "credZipCode": "43359",
      "credModelDetail": "02",
      "credNameOnBpkb": "name bpkb",
      "credMaidenName": "ibuk",
      "credObbrCode": _providerDataUnit.idMerkKendaraan,
      "credDateSurvey": 1613727706253,
      "credBrId": "0321",
      "credFinType": 0,
      "credPrincipalAmt": 0,
      "credSvyKabKotaId": _providerDataDomisili.idKota,
      "credApplSourceCode": "01",
      "credEconomySector": "0001",
      "credTimeMinuteSurvey": "19:19",
      "credSvyProvinceId": _providerDataDomisili.idKotaProvinsi,
      "credObjtFotoSisi42": "42",
      "credSvyKelurahanId": _providerDataDomisili.idKelurahan,
      "credObjtFotoSisi4": "4",
      "credMarriageStatus": "001",
      "credObjtFotoSisi43": "43",
      "credObjtFotoSisi44": "44",
      "credInsuranceType": "1",
      "credSvyKecamatanId": _providerDataDomisili.idKecamatan,
      "credOrderNo": OrderNo,
      "createdBy": "10991431"
    };
    final _result = await _OrderApiProvider.SubmitOrder(data);
    print(_result.toString());
    if (_result['status'] == true) {
      print("masuk");
      print(_result['data'][0]);
      setState(() {
        _processSendData = false;
      });
      // Navigator.pop(context);
      _showDialogSuccess(_result['data'][0]);
      // _validatesubmit = true;
      // _isProcessCheckVoucher = false;
      // showDialog(
      //     context: context,
      //     builder: (_) => new AlertDialog(
      //           title: new Text("Material Dialog"),
      //           content: new Text("Hey! I'm Coflutter!"),
      //           actions: <Widget>[
      //             FlatButton(
      //               child: Text('Close me!'),
      //               onPressed: () {
      //                 Navigator.pop(context);
      //                 Navigator.push(context,
      //                     MaterialPageRoute(builder: (context) => HomePage()));
      //               },
      //             )
      //           ],
      //         ));
    } else {
      setState(() {
        _processSendData = false;
      });
      Navigator.pop(context);
      _showDialogError(_result['message']);
      // _validatesubmit = false;
    }
  }

  _showDialogSuccess(Map message) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Success", style: TextStyle(fontFamily: "NunitoSans")),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(width: 65, child: Text("Order No")),
                    Text(" : "),
                    Text(message['credOrderNo'] != null
                        ? message['credOrderNo']
                        : ""),
                  ],
                ),
                SizedBox(
                  height: size.hp(2),
                ),
                Row(
                  children: <Widget>[
                    Container(width: 65, child: Text("Appl No")),
                    Text(" : "),
                    Text(message['credAppsNo'] != null
                        ? message['credAppsNo']
                        : ""),
                  ],
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Provider.of<SubmitDataConsumen>(context, listen: false)
                        .clearDataCustomer();
                    Provider.of<SubmitDomisiliNotifier>(context, listen: false)
                        .clearDataDomisili();
                    Provider.of<SubmitUnitNotifier>(context, listen: false)
                        .clearDataUnit();
                    Navigator.pop(context);
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomePage(
                                  dif: 1,
                                )),
                        (Route<dynamic> route) => false);
                  },
                  child: Text("Close",
                      style: TextStyle(
                          fontFamily: "NunitoSans", color: Colors.yellow))),
            ],
          );
        });
  }

  _showDialogError(String message) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(message, style: TextStyle(fontFamily: "NunitoSans")),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    // setState(() {
                    //   _pemilihanCaraSurvey = _listPemilihanCaraSurvey[0];
                    // });
                  },
                  child: Text("Close",
                      style: TextStyle(
                          fontFamily: "NunitoSans", color: Colors.yellow))),
            ],
          );
        });
  }
}
