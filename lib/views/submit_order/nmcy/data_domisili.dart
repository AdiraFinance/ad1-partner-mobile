import 'package:adira_one_partner/components/tf.dart';
import 'package:adira_one_partner/services/submit_domisili_notifier.dart';
import 'package:flutter/material.dart';

// import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:adira_one_partner/services/order_api_provider.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:adira_one_partner/views/modal/modal_order/modal_data_kecamatan.dart';
import 'package:provider/provider.dart';

class DataDomisili extends StatefulWidget {
  @override
  _DataDomisiliState createState() => _DataDomisiliState();
}

class _DataDomisiliState extends State<DataDomisili> {
  bool isEnabled = true;
  final _key = GlobalKey<FormState>();
  bool _autoValidate = false;
  OrderApiProvider _OrderApiProvider;
  String CondTime;
  String Time;
  int _rbDomisili = 0;

  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now()..add(new Duration(days: 7));

  @override
  void initState() {
    super.initState();
    _OrderApiProvider = OrderApiProvider();
    isEnabled = true;
    Time =
        DateTime.now().hour.toString() + '.' + DateTime.now().minute.toString();
    CondTime = DateFormat('yyyy-MM-dd').format(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SubmitDomisiliNotifier>(
        builder: (_, data, __) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Alamat Domisili",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                Divider(
                  color: Colors.black,
                  height: 36,
                ),
                Text("Alamat Domisili Sama Dengan Alamat KTP?",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Radio(
                        activeColor: Colors.green,
                        value: 0,
                        groupValue: _rbDomisili,
                        onChanged: _handleRadioCustTypeValueChange),
                    Text(
                      "Ya",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Nunito Sans"),
                    ),
                    Radio(
                        activeColor: Colors.green,
                        value: 1,
                        groupValue: _rbDomisili,
                        onChanged: _handleRadioCustTypeValueChange),
                    Text(
                      "Tidak",
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          fontFamily: "Nunito Sans"),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Text("Alamat Domisili",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                SizedBox(height: 20),
                AppTextFormField(
                  controller: data.controllerAlamatDomisili,
                  hintText: 'Nama jalan dan nomor rumah',
                  enabled: true,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                ),
                SizedBox(height: 12),
                Text("Kelurahan Domisili",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                SizedBox(height: 20),
                AppTextFormField(
                  controller: data.controllerKelurahanAll,
                  hintText: 'Ketikan Kelurahan',
                  enabled: true,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  suffixIcon: Icons.search,
                  onTap: () async {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ExamplePageState(
                            onSelected: data.setValueKecamatan)));
                  },
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Checkbox(
                      checkColor: Colors.white,
                      activeColor: Colors.greenAccent,
                      value: data.checkKelurahan,
                      onChanged: (value) {
                        setState(() {
                          data.checkKelurahan = !data.checkKelurahan;
                        });
                      },
                    ),
                    Text("Centang jika kelurahan tidak di temukan",
                        style: TextStyle(fontSize: 14)),
                  ],
                ),
                SizedBox(height: 24),
                if (data.checkKelurahan == true)
                  Text("Catatan Kepada Sales Officer",
                      style: TextStyle(fontWeight: FontWeight.bold)),
                if (data.checkKelurahan == true)
                  Column(
                    children: [
                      SizedBox(height: 20),
                      AppTextFormField(
                        controller: data.controllerKelurahanAdd,
                        hintText: 'Nama jalan dan nomor rumah',
                        enabled: true,
                        keyboardType: TextInputType.text,
                        textInputAction: TextInputAction.done,
                      ),
                      SizedBox(height: 24),
                    ],
                  ),
                Text("Jadwal Survey",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
                Divider(
                  color: Colors.black,
                  height: 36,
                ),
                Text("Pilih Tanggal Survey",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                SizedBox(height: 24),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Color(0xffF6F6F6),
                  ),
                  child: Container(
                    color: Color(0xffF6F6F6),
                    alignment: Alignment.center,
                    child: DatePicker(
                      DateTime.now(),
                      width: 100,
                      height: 100,
                      selectedTextColor: Color(0xFF3AB57C),
                      selectionColor: Colors.greenAccent,
                      deactivatedColor: Colors.grey,
                      locale: "id_ID",
                      onDateChange: (date) {
                        // New date selected
                        data.controllerSelectedDate.text =
                            DateFormat.MMMMEEEEd("id_ID").format(date);
                        CondTime = DateFormat('yyyy-MM-dd').format(date);
                        data.controllerNotifDate.text = "Anda Memilih " +
                            data.controllerSelectedDate.text +
                            ". Pkl " +
                            data.controllerJamSurvey.text;
                      },
                    ),
                  ),
                ),
                SizedBox(height: 24),
                new Row(
                  children: <Widget>[
                    new Flexible(
                      child: Text("Pilih Jam Survey",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18)),
                    ),
                    SizedBox(width: 30),
                    new Flexible(
                      child: TextField(
                          //       autovalidate: _autoValidate3,
                          controller: data.controllerJamSurvey,
                          readOnly: true,
                          decoration: new InputDecoration(
                              filled: true,
                              fillColor: Color(0xffF6F9FE),
                              labelText: 'Pilih',
                              labelStyle: TextStyle(
                                  fontFamily: "NunitoSans", color: Colors.grey),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              suffixIcon: Icon(Icons.keyboard_arrow_down_sharp,
                                  color: Colors.blue)),
                          onTap: () {
                            /* showDialog(
                      context: context,
                      builder: (BuildContext context) => _buildPopupDialog(context),
                    ); */
                            showBarModalBottomSheet(
                              expand: true,
                              context: context,
                              backgroundColor: Colors.transparent,
                              builder: (context) => _buildPopupDialog(context),
                            );
                          }),
                    ),
                  ],
                ),
                SizedBox(height: 24),
                AppTextFormField(
                  controller: data.controllerNotifDate,
                  enabled: false,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                ),
                SizedBox(height: 24),
              ],
            ));
  }

  Widget TimeWidget(String param) {
    var Now  = DateTime.now();
    Time =
        Now.hour.toString() + '.' + Now.minute.toString();
    var parameter = param.split('.');
    int hour = int.parse(parameter[0]);
    int minute = int.parse(parameter[1]);
    DateTime datecond = new DateTime(Now.year, Now.month, Now.day, hour, minute);

    if (Now.isAfter(datecond) &&
        CondTime == DateFormat('yyyy-MM-dd').format(Now))
      return FlatButton(
        onPressed: null,
        child: Text(param + ' WIB',
            style: TextStyle(color: Colors.grey, fontSize: 14)),
      );
    else
      return FlatButton(
        onPressed: () {
          Provider.of<SubmitDomisiliNotifier>(context, listen: false)
              .setValueNotif(param + ' WIB');
          Navigator.pop(context);
        },
        child: Text(param + ' WIB',
            style: TextStyle(color: Colors.black, fontSize: 14)),
      );
  }

  Widget _buildPopupDialog(BuildContext context) {
    return new SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Pilih Jam Survey",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
            SizedBox(height: 30),
            Text(
              "Pagi",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            Row(
              children: [
                TimeWidget('07.00'),
                TimeWidget('08.30'),
                TimeWidget('10.00'),
              ],
            ),
            Row(
              children: [
                TimeWidget('07.30'),
                TimeWidget('09.00'),
                TimeWidget('10.30'),
              ],
            ),
            Row(
              children: [
                TimeWidget('08.00'),
                TimeWidget('09.30'),
                TimeWidget('11.00'),
              ],
            ),
            Divider(
              color: Colors.black,
              height: 36,
            ),
            Text(
              "Siang",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            Row(
              children: [
                TimeWidget('11.30'),
                TimeWidget('13.00'),
                TimeWidget('14.30'),
              ],
            ),
            Row(
              children: [
                TimeWidget('12.00'),
                TimeWidget('13.30'),
                TimeWidget('15.00')
              ],
            ),
            Row(
              children: [
                TimeWidget('12.30'),
                TimeWidget('14.00'),
                TimeWidget('15.30'),
              ],
            ),
            Divider(
              color: Colors.black,
              height: 36,
            ),
            Text(
              "Sore & Malam",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            Row(
              children: [
                TimeWidget('16.00'),
                TimeWidget('18.30'),
                TimeWidget('21.00'),
              ],
            ),
            Row(
              children: [
                TimeWidget('16.30'),
                TimeWidget('19.00'),
                TimeWidget('21.30'),
              ],
            ),
            Row(
              children: [
                TimeWidget('17.00'),
                TimeWidget('19.30'),
                TimeWidget('22.00'),
              ],
            ),
            Row(
              children: [
                TimeWidget('17.30'),
                TimeWidget('20.00'),
              ],
            ),
            Row(
              children: [
                TimeWidget('18.00'),
                TimeWidget('20.30'),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _handleRadioCustTypeValueChange(int value) {
    setState(() {
      _rbDomisili = value;
      print(_rbDomisili);
      if (_rbDomisili == 0) {
        // Provider.of<SubmitDomisiliNotifier>(context, listen: false)
        //         .controllerAlamatDomisili
        //         .text =
        //     Provider.of<SubmitDomisiliNotifier>(context, listen: false)
        //         .controllerAlamatOcr
        //         .text;
      } else {
        // Provider.of<SubmitDomisiliNotifier>(context, listen: false)
        //     .controllerAlamatDomisili
        //     .clear();
      }
    });
  }
}
