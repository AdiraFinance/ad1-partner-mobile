import 'dart:math';

import 'package:adira_one_partner/components/tf.dart';
import 'package:adira_one_partner/services/submit_unit_notifier.dart';
import 'package:adira_one_partner/utils/currency.dart';
import 'package:adira_one_partner/views/modal/modal_order/search_model_kendaraan.dart';
import 'package:adira_one_partner/widgets/decimal_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class DataUnit extends StatefulWidget {
  @override
  _DataUnitState createState() => _DataUnitState();
}

class _DataUnitState extends State<DataUnit> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final formatCurrency =
      new NumberFormat.simpleCurrency(decimalDigits: 0, locale: "id_ID");

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<SubmitUnitNotifier>(
        builder: (_, data, __) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Unit Kendaraan",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                Divider(
                  color: Colors.black,
                  height: 36,
                ),
                // SizedBox(height: 20),
                Text("Tipe Unit",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
                SizedBox(height: 5),
                AppTextFormField(
                  controller: data.controllerTipeKendaraanAll,
                  hintText: 'Ketik Merek, tipe unit dan tahun',
                  enabled: true,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  onTap: () async {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => SearchModelKendaraan(
                            onSelected: data.setValueModel,
                            idStatusKendaraan: "001")));
                  },
                  suffixIcon: Icons.search,
                ),
                SizedBox(height: 12),
                Text("Tuliskan merek dan tipe unit jika tidak ditemukan",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: Colors.grey)),
                SizedBox(height: 16),
                Text("Pembiayaan",
                    style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
                Divider(
                  color: Colors.black,
                  height: 36,
                ),
                // SizedBox(height: 12),
                Text("OTR",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 14)),
                SizedBox(height: 5),
                AppTextFormField(
                  controller: data.controllerOtr,
                  labelText: 'Rp',
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.done,
                  inputFormatter: [
                    FilteringTextInputFormatter.digitsOnly,
                    CurrencyFormat()
                    // formMIncomeChangeNotif.amountValidator
                  ],
                ),
                SizedBox(height: 10),
                new Row(
                  children: <Widget>[
                    new Flexible(
                      child: Text("Tenor Dalam Bulan",
                          style: TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 12)),
                    ),
                    SizedBox(width: 85),
                    new Flexible(
                      child: Text("DP",
                          style: TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 12)),
                    ),
                  ],
                ),
                SizedBox(height: 5),
                new Row(
                  children: <Widget>[
                    new Flexible(
                      child: TextFormField(
                        controller: data.controllerTenor,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Ketik tenor',
                          labelStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.white)),
                        ),
                      ),
                    ),
                    SizedBox(width: 30),
                    new Flexible(
                      child: TextFormField(
                        controller: data.controllerDp,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Rp',
                          labelStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.white)),
                        ),
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly,
                          CurrencyFormat()
                          // formMIncomeChangeNotif.amountValidator
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Text("Cicilan",
                    style:
                        TextStyle(fontWeight: FontWeight.w700, fontSize: 12)),
                SizedBox(height: 5),
                TextFormField(
                  controller: data.controllerCicilan,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Rp',
                    labelStyle: TextStyle(color: Colors.grey),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: BorderSide(color: Colors.white)),
                  ),
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                    CurrencyFormat()
                    // formMIncomeChangeNotif.amountValidator
                  ],
                ),
                SizedBox(height: 24),
              ],
            ));
  }
}
