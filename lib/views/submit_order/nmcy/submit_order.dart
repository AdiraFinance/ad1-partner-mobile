import 'package:adira_one_partner/services/submit_data_konsumen_notifier.dart';
import 'package:adira_one_partner/utils/constanta.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_domisili.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_unit.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/preview_page.dart';
import 'package:flutter/material.dart';
import 'package:pdf/widgets/basic.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_konsumen.dart';
import 'package:provider/provider.dart';

class DataKonsumen extends StatefulWidget {
  @override
  _DataKonsumenState createState() => _DataKonsumenState();
}

class _DataKonsumenState extends State<DataKonsumen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pengajuan Kredit',
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        // primarySwatch: kPrimaryColor, //Colors.amber,
        accentColor: Colors.black,
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentStep = 0;
  StepperType stepperType = StepperType.horizontal;
  VoidCallback _onStepContinue, _onStepCancel;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Row(
          children: [
            IconButton(
                alignment: Alignment.centerRight,
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  _onStepCancel();
                }),
            Text("Order Motor Baru"),
          ],
        ),
        automaticallyImplyLeading: true,
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Stepper(
                type: stepperType,
                physics: ScrollPhysics(),
                currentStep: _currentStep,
                onStepTapped: (step) => tapped(step),
                onStepContinue: continued,
                onStepCancel: cancel,
                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  _onStepContinue = onStepContinue;
                  _onStepCancel = onStepCancel;
                  return Container();
                  // SizedBox(
                  //   width: 0,
                  //   height: 0,
                  // );
                },
                steps: <Step>[
                  Step(
                    title: new Text('Data Konsumen'),
                    content: InputDataNasabah(),
                    isActive: _currentStep >= 0,
                    state: _currentStep >= 0
                        ? StepState.complete
                        : StepState.disabled,
                  ),
                  Step(
                    title: new Text('Domisili & Survey'),
                    content: DataDomisili(),
                    isActive: _currentStep >= 1,
                    state: _currentStep >= 1
                        ? StepState.complete
                        : StepState.disabled,
                  ),
                  Step(
                    title: new Text('Pembiayaan'),
                    content: DataUnit(),
                    isActive: _currentStep >= 2,
                    state: _currentStep >= 2
                        ? StepState.complete
                        : StepState.disabled,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ButtonBar(
        children: [
          _currentStep == 2
              ? Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  // padding: EdgeInsets.only(bottom: 5),
                  height: 80,
                  width: double.maxFinite,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: kPrimaryColor,
                            onPrimary: Colors.black,
                            // elevation: 10,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0))),
                        onPressed: () {
                          // _onStepContinue();
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => PreviewPage()));
                        },
                        child: Text(
                          "Simpan & Lanjutkan",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                      ),
                      Container(
                        height: 30,
                        alignment: Alignment.center,
                        child: TextButton(
                          onPressed: () {},
                          child: Text(
                            "Simpan ke Draft",
                            style: TextStyle(
                              fontSize: 15,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  // padding: EdgeInsets.only(bottom: 5),
                  height: 80,
                  width: double.maxFinite,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: kPrimaryColor,
                            onPrimary: Colors.black,
                            // elevation: 10,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(30.0))),
                        onPressed: () {
                          // _onStepContinue();
                          check();
                        },
                        child: Text(
                          "Simpan & Lanjutkan",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                      ),
                      Container(
                        height: 30,
                        alignment: Alignment.center,
                        child: TextButton(
                          onPressed: () {},
                          child: Text(
                            "Simpan ke Draft",
                            style: TextStyle(
                              fontSize: 15,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
        ],
      ),
    );
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    _currentStep < 2 ? setState(() => _currentStep += 1) : null;
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }

  check() {
    if (_currentStep == 0) {
      setState(() {
        var _providerDataCustomer =
            Provider.of<SubmitDataConsumen>(context, listen: false);
        print(_providerDataCustomer.nikController.value);
        print(_providerDataCustomer.fullNameController.value);
        if (_providerDataCustomer.nikController.text == "" ||
            _providerDataCustomer.fullNameController.text == "") {
          _showSnackBar('Mohon dicek kembali datanya');
        } else {
          _onStepContinue();
        }
      });
    } else {
      _onStepContinue();
    }
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(text), behavior: SnackBarBehavior.floating));
  }
}
