import 'package:flutter/material.dart';
import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:adira_one_partner/services/order_api_provider.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:adira_one_partner/views/modal/modal_order/modal_data_kecamatan.dart';

class DataDomisili extends StatefulWidget {
  @override
  _DataDomisiliState createState() => _DataDomisiliState();
}

class _DataDomisiliState extends State<DataDomisili> {
  final _key = GlobalKey<FormState>();
  bool _autoValidate = false;
  final _Search = TextEditingController();
  final datakec = [];
  final _JamSurveyController = TextEditingController();
  OrderApiProvider _OrderApiProvider;
  ExamplePageState _ExamplePageState;
  final _NotifController = TextEditingController();
  final _SelectedDate = TextEditingController();
  int _RBDomisili = 0;
  final _KecamatanId = TextEditingController();
  final _KelurahanId = TextEditingController();
  final _KotaId = TextEditingController();
  final _AlamatDomisili = TextEditingController();
  bool _checkkeluahan = false;

  var idKelurahan, idKecamatan, idKota, idKotaProvinsi;

  final _controllerKecamatan = TextEditingController();
  final _controllerKelurahan = TextEditingController();
  final _controllerKodePos = TextEditingController();
  final _controllerProvinsi = TextEditingController();
  final _controllerKota = TextEditingController();
  final _controllerKelurahanAll = TextEditingController();

  String _textKota, _textProv;
  setValueKecamatan(Map value) {
    print("cek kecamatan dkk $value");
    setState(() {
      _controllerKelurahanAll.text =
          value['PARA_KELURAHAN_DESC'].toString().trim() +
              ', ' +
              value['PARA_KECAMATAN_DESC'].toString().trim() +
              ', ' +
              value['PROVINSI_DESC'].toString().trim();
      _controllerKota.text = value['KAB_KOT_DESC'];
      _controllerKecamatan.text = value['PARA_KECAMATAN_DESC'];
      _controllerKelurahan.text = value['PARA_KELURAHAN_DESC'];
      _controllerKodePos.text = value['PARA_KELURAHAN_ZIP_CODE'];
      _controllerKota.text = value['KAB_KOT_DESC'];
      _controllerProvinsi.text = value['PROVINSI_DESC'];
      idKecamatan = value['PARA_KELURAHAN_ID_KEC'];
      idKelurahan = value['PARA_KELURAHAN_ID'];
      idKota = value['KAB_KOT_ID'];
      idKotaProvinsi = value['KAB_KOT_ID_PROV'];
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Alamat Domisili",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
        Divider(
          color: Colors.black,
          height: 36,
        ),
        Text("Alamat Domisili Sama Dengan Alamat KTP?",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
        Row(
          children: <Widget>[
            new Radio(
              activeColor: Colors.greenAccent,
              value: 0,
              groupValue: _RBDomisili,
              onChanged: _handleRadioValueChange,
            ),
            new Text(
              'Ya',
              style: new TextStyle(fontSize: 16.0),
            ),
            new Radio(
              activeColor: Colors.greenAccent,
              value: 1,
              groupValue: _RBDomisili,
              onChanged: _handleRadioValueChange,
            ),
            new Text(
              'Tidak',
              style: new TextStyle(
                fontSize: 16.0,
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Text("Alamat Domisili",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
        SizedBox(height: 20),
        TextFormField(
          keyboardType: TextInputType.text,
          controller: _AlamatDomisili,
          decoration: InputDecoration(
            labelText: 'Nama jalan dan nomor rumah',
            labelStyle: TextStyle(color: Colors.grey),
            filled: true,
            fillColor: Color(0xffF6F9FE),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.white)),
          ),
        ),
        SizedBox(height: 12),
        Text("Kelurahan Domisili",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
        SizedBox(height: 20),
        TextFormField(
          //       autovalidate: _autoValidate3,
          controller: _controllerKelurahanAll,
          style: TextStyle(fontSize: 14),
          onTap: () async {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>
                    ExamplePageState(onSelected: setValueKecamatan)));
            // Navigator.push(
            //   context,
            //   MaterialPageRoute(builder: (context) => ExamplePageState()),
            // );
          },
          decoration: new InputDecoration(
              filled: true,
              fillColor: Color(0xffF6F9FE),
              labelText: 'Ketikan Kelurahan',
              labelStyle:
                  TextStyle(fontFamily: "NunitoSans", color: Colors.grey),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              suffixIcon: Icon(Icons.search, color: Colors.greenAccent)),
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Checkbox(
              checkColor: Colors.white,
              activeColor: Colors.greenAccent,
              value: _checkkeluahan,
              onChanged: (value) {
                setState(() {
                  _checkkeluahan = !_checkkeluahan;
                });
              },
            ),
            Text("Centang jika kelurahan tidak di temukan",
                style: TextStyle(fontSize: 14)),
          ],
        ),
        SizedBox(height: 24),
        if (_checkkeluahan == true)
          Text("Catatan Kepada Sales Officer",
              style: TextStyle(fontWeight: FontWeight.bold)),
        if (_checkkeluahan == true)
          Column(
            children: [
              SizedBox(height: 20),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: _AlamatDomisili,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Color(0xffF6F9FE),
                  labelText:
                      'Tulis kelurahan dan kota domisili konsumen. Contoh Cipulir, Jakarta Selatan',
                  labelStyle: TextStyle(color: Colors.grey),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(color: Colors.white)),
                ),
              ),
              SizedBox(height: 24),
            ],
          ),

        Text("Jadwal Survey",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
        Divider(
          color: Colors.black,
          height: 36,
        ),
        Text("Pilih Tanggal Survey",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
        SizedBox(height: 24),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Color(0xffEFEFEF),
          ),
          child: DatePicker(
            DateTime.now(),
            width: 100,
            height: 100,
            selectedTextColor: Color(0xFF3AB57C),
            selectionColor: Colors.white,
            deactivatedColor: Colors.grey,
            locale: "id_ID",
            onDateChange: (date) {
              // New date selected
              _SelectedDate.text = DateFormat.MMMMEEEEd("id_ID").format(date);
              _NotifController.text = "Anda Memilih " +
                  _SelectedDate.text +
                  ". Pkl " +
                  _JamSurveyController.text;
            },
          ),
        ),
        SizedBox(height: 24),
        new Row(
          children: <Widget>[
            new Flexible(
              child: Text("Pilih Jam Survey",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
            ),
            SizedBox(width: 30),
            new Flexible(
              child: TextField(
                  //       autovalidate: _autoValidate3,
                  controller: _JamSurveyController,
                  readOnly: true,
                  decoration: new InputDecoration(
                      filled: true,
                      fillColor: Color(0xffF6F9FE),
                      labelText: 'Pilih',
                      labelStyle: TextStyle(
                          fontFamily: "NunitoSans", color: Colors.grey),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      suffixIcon: Icon(Icons.keyboard_arrow_down_sharp,
                          color: Colors.blue)),
                  onTap: () {
                    /* showDialog(
                      context: context,
                      builder: (BuildContext context) => _buildPopupDialog(context),
                    ); */
                    showBarModalBottomSheet(
                      expand: true,
                      context: context,
                      backgroundColor: Colors.transparent,
                      builder: (context) => _buildPopupDialog(context),
                    );
                  }),
            ),
          ],
        ),
        SizedBox(height: 24),
        TextFormField(
          //       autovalidate: _autoValidate3,
          textAlign: TextAlign.center,
          controller: _NotifController,
          enabled: false,
          style: TextStyle(
            fontSize: 15,
          ),
          decoration: new InputDecoration(
            filled: true,
            fillColor: Colors.grey[200],
            labelStyle: TextStyle(
                fontFamily: "NunitoSans", color: Colors.grey, fontSize: 8),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
          ),
        ),
        SizedBox(height: 24),
        // RaisedButton(
        //   shape:
        //       RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        //   onPressed: () {
        //     // latihanDay1.check(context);
        //     _check();
        //   },
        //   color: Colors.yellow,
        //   child: Row(
        //     mainAxisSize: MainAxisSize.max,
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       Text(
        //         "Simpan & Lanjutkan",
        //         style: TextStyle(color: Colors.black, fontSize: 20),
        //       ),
        //     ],
        //   ),
        // ),
        SizedBox(height: 20),
        new Row(
          children: <Widget>[
            new Flexible(
              child: Center(
                child: Text(
                  "Simpan Ke Draft",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blue[700], fontSize: 20),
                ),
              ),
            ),
            SizedBox(width: 30),
          ],
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _OrderApiProvider = OrderApiProvider();
    _NotifController.text = "Anda Belum Memilih Jadwal Survey";
  }

  _SearchKecamatan() async {
    //datakec.clear();
    final _result = await _OrderApiProvider.SearchKecamatan(_Search.text);
    for (var i = 0; i < _result['Data'].length; i++) {
      datakec.add(_result['Data'][i]);
    }
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _RBDomisili = value;

      switch (_RBDomisili) {
        case 0:
          break;
        case 1:
          break;
        case 2:
          break;
      }
    });
  }

  void _SetValueNotif(String param) {
    _JamSurveyController.text = param;
    _NotifController.text = "Anda Memilih " +
        _SelectedDate.text +
        ". Pkl " +
        _JamSurveyController.text;

    Navigator.pop(context);
  }

  Widget _buildPopupDialog(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Pilih Jam Survey",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          SizedBox(height: 30),
          Text(
            "Pagi",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("07.00 WIB");
                },
                child: const Text('07.00 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("08.30 WIB");
                },
                child: const Text(
                  '08.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("10.00 WIB");
                },
                child: const Text(
                  '10.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("07.30 WIB");
                },
                child: const Text('07.30 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("09.00 WIB");
                },
                child: const Text(
                  '09.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("10.30 WIB");
                },
                child: const Text(
                  '10.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("08.00 WIB");
                },
                child: const Text('08.00 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("09.30 WIB");
                },
                child: const Text(
                  '09.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("11.00 WIB");
                },
                child: const Text(
                  '11.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Text(
            "Siang",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("11.30 WIB");
                },
                child: const Text('11.30 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("13.00 WIB");
                },
                child: const Text(
                  '13.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("14.30 WIB");
                },
                child: const Text(
                  '14.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("12.00 WIB");
                },
                child: const Text('12.00 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("13.30 WIB");
                },
                child: const Text(
                  '13.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("15.00 WIB");
                },
                child: const Text(
                  '15.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("12.30 WIB");
                },
                child: const Text('12.30 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("14.00 WIB");
                },
                child: const Text(
                  '14.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("15.30 WIB");
                },
                child: const Text(
                  '15.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Text(
            "Sore & Malam",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("16.00 WIB");
                },
                child: const Text('16.00 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("18.30 WIB");
                },
                child: const Text(
                  '18.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("21.00 WIB");
                },
                child: const Text(
                  '21.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("16.30 WIB");
                },
                child: const Text('16.30 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("19.00 WIB");
                },
                child: const Text(
                  '19.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("21.30 WIB");
                },
                child: const Text(
                  '21.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("17.00 WIB");
                },
                child: const Text('17.00 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("19.30 WIB");
                },
                child: const Text(
                  '19.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("22.00 WIB");
                },
                child: const Text(
                  '22.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("17.30 WIB");
                },
                child: const Text('17.30 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("20.00 WIB");
                },
                child: const Text(
                  '20.00 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
          Row(
            children: [
              FlatButton(
                onPressed: () {
                  _SetValueNotif("18.00 WIB");
                },
                child: const Text('18.00 WIB',
                    style: TextStyle(color: Colors.black, fontSize: 14)),
              ),
              FlatButton(
                onPressed: () {
                  _SetValueNotif("20.30 WIB");
                },
                child: const Text(
                  '20.30 WIB',
                  style: TextStyle(color: Colors.black, fontSize: 14),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
