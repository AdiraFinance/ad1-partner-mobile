import 'dart:math';

import 'package:adira_one_partner/views/modal/modal_order/search_model_kendaraan.dart';
import 'package:flutter/material.dart';

class DataUnit extends StatefulWidget {
  @override
  _DataUnitState createState() => _DataUnitState();
}

class _DataUnitState extends State<DataUnit> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final _controllerModelKendaraan = new TextEditingController();
  final _controllerTipeKendaraan = new TextEditingController();
  final _controllerTipeKendaraanAll = new TextEditingController();
  final _controllerMerkKendaraan = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Unit Kendaraan",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
        Divider(
          color: Colors.black,
          height: 36,
        ),
        // SizedBox(height: 20),
        Text("Merk Unit",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
        SizedBox(height: 5),
        TextFormField(
          //       autovalidate: _autoValidate3,
          controller: _controllerTipeKendaraanAll,
          onTap: () async {
            // Navigator.of(context).push(MaterialPageRoute(
            //     builder: (context) => SearchModelKendaraan(
            //         onSelected: _setValueModel, idStatusKendaraan: "001")));
          },
          decoration: new InputDecoration(
              labelText: 'Ketik Merek, tipe unit dan tahun',
              labelStyle:
                  TextStyle(fontFamily: "NunitoSans", color: Colors.grey),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              suffixIcon: Icon(Icons.search, color: Colors.blue)),
        ),
        SizedBox(height: 12),
        Text("Tipe Unit",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)),
        SizedBox(height: 5),
        TextFormField(
          //       autovalidate: _autoValidate3,
          controller: _controllerTipeKendaraanAll,
          onTap: () async {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => SearchModelKendaraan(
                    onSelected: _setValueModel, idStatusKendaraan: "001")));
          },
          decoration: new InputDecoration(
              labelText: 'Ketik Merek, tipe unit dan tahun',
              labelStyle:
                  TextStyle(fontFamily: "NunitoSans", color: Colors.grey),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              suffixIcon: Icon(Icons.search, color: Colors.blue)),
        ),
        SizedBox(height: 16),
        Text("Pembiayaan",
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18)),
        Divider(
          color: Colors.black,
          height: 36,
        ),
        // SizedBox(height: 12),
        Text("Harga Barang",
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 14)),
        SizedBox(height: 5),
        TextFormField(
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: 'Rp',
            labelStyle: TextStyle(color: Colors.grey),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.white)),
          ),
        ),
        SizedBox(height: 10),
        new Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Flexible(
              child: Text("Tenor Dalam Bulan",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 12)),
            ),
            SizedBox(width: 85),
            new Flexible(
              child: Text("Rate",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 12)),
            ),
          ],
        ),
        SizedBox(height: 5),
        new Row(
          children: <Widget>[
            new Flexible(
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Ketik tenor',
                  labelStyle: TextStyle(color: Colors.grey),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(color: Colors.white)),
                ),
              ),
            ),
            SizedBox(width: 30),
            new Flexible(
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Rp',
                  labelStyle: TextStyle(color: Colors.grey),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      borderSide: BorderSide(color: Colors.white)),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 20),
        Text("DP", style: TextStyle(fontWeight: FontWeight.w700, fontSize: 12)),
        SizedBox(height: 5),
        TextFormField(
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            labelText: 'Rp',
            labelStyle: TextStyle(color: Colors.grey),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.white)),
          ),
        ),
        SizedBox(height: 20),
        Text("Cicilan Per Bulan",
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 12)),
        SizedBox(height: 5),
        TextFormField(
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            labelText: 'Rp',
            labelStyle: TextStyle(color: Colors.grey),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.white)),
          ),
        ),
        SizedBox(height: 24),
        // RaisedButton(
        //   shape:
        //       RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        //   onPressed: () {
        //     // latihanDay1.check(context);
        //     // _check();
        //   },
        //   color: Colors.yellow,
        //   child: Row(
        //     mainAxisSize: MainAxisSize.max,
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: [
        //       Text(
        //         "Simpan & Lanjutkan",
        //         style: TextStyle(color: Colors.black, fontSize: 20),
        //       ),
        //     ],
        //   ),
        // ),
        SizedBox(height: 20),
        new Row(
          children: <Widget>[
            new Flexible(
              child: Center(
                child: Text(
                  "Simpan Ke Draft",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.blue[700], fontSize: 20),
                ),
              ),
            ),
            SizedBox(width: 30),
          ],
        ),
      ],
    );
  }

  var idMerkKendaraan;
  var idTypeKendaraan;
  var idModelKendaraan;

  _setValueModel(Map _modelKendaraan) {
    print("data model kendaraan $_modelKendaraan");
    setState(() {
      idMerkKendaraan = _modelKendaraan['OBBR_CODE'];
      idTypeKendaraan = _modelKendaraan['OBTY_CODE'];
      idModelKendaraan = _modelKendaraan['OBMO_CODE'];
      _controllerModelKendaraan.text = _modelKendaraan['OBBR_DESC'];
      _controllerTipeKendaraan.text = _modelKendaraan['OBTY_DESC'];
      _controllerMerkKendaraan.text = _modelKendaraan['OBMO_DESC'];
      _controllerTipeKendaraanAll.text =
          _modelKendaraan['OBBR_DESC'].toString().trim() +
              ', ' +
              _modelKendaraan['OBTY_DESC'].toString().trim() +
              ', ' +
              _modelKendaraan['OBMO_DESC'].toString().trim();
    });
  }
}
