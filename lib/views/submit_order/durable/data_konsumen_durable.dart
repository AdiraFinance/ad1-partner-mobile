import 'dart:io';

import 'package:adira_one_partner/services/submit_data_konsumen_notifier.dart';
import 'package:adira_one_partner/widgets/foto_camera.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class InputDataNasabah extends StatefulWidget {
  @override
  InputDataNasabahState createState() => InputDataNasabahState();
}

class InputDataNasabahState extends State<InputDataNasabah> {
  bool _disable = false;

  var nikController = TextEditingController();
  var fullNameController = TextEditingController();
  var phoneNoController = TextEditingController();
  var waNoController = TextEditingController();
  int _radioCustTypeValue = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Tipe Konsumen",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Radio(
                value: 0,
                groupValue: _radioCustTypeValue,
                onChanged: _handleRadioCustTypeValueChange),
            Text(
              "Individu",
              style: TextStyle(fontSize: 15),
            ),
            Radio(
                value: 1,
                groupValue: _radioCustTypeValue,
                onChanged: _handleRadioCustTypeValueChange),
            Text(
              "Perusahaan",
              style: TextStyle(fontSize: 15),
            ),
          ],
        ),
        SizedBox(
          height: 6,
        ),
        Text(
          "Data Konsumen",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
        SizedBox(
          height: 6,
        ),
        Container(
          decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey[300],
                width: 1,
              ),
              borderRadius: BorderRadius.circular(8)),
          padding: EdgeInsets.all(6),
          child: Consumer<SubmitDataConsumen>(
            builder: (_, image, __) => ListTile(
              leading: image.imageFile == null
                  ? Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Icon(Icons.camera_enhance),
                    )
                  : Container(
                      width: 70,
                      height: 70,
                      child: Image.file(image.imageFile, fit: BoxFit.cover),
                    ),
              title: Align(
                alignment: Alignment.centerLeft,
                child: TextButton(
                  onPressed: () => showDialog(
                    context: context,
                    builder: (BuildContext ctx) => _dialogPopUpCamera(ctx),
                  ),
                  child: image.imageFile == null
                      ? Text('Unggah Foto KTP')
                      : Text('KTP Sukses diunggah'),
                ),
              ),
              subtitle: image.imageFile == null
                  ? Text('Silahkan unggah ktp konsumen')
                  : Text('Ketuk untuk lihat'),
              trailing: image.imageFile != null
                  ? IconButton(
                      color: Theme.of(context).errorColor,
                      icon: Icon(Icons.delete),
                      onPressed: () => Provider.of<SubmitDataConsumen>(context,
                              listen: false)
                          .dispose(),
                    )
                  : null,
            ),
          ),
        ),
        SizedBox(
          height: 6,
        ),
        Text(
          "NIK",
          style: TextStyle(fontSize: 15),
        ),
        SizedBox(
          height: 6,
        ),
        Consumer<SubmitDataConsumen>(
          builder: (_, nik, __) => TextFormField(
            controller: nik.nikController,
            onSaved: (newValue) {
              nik.setNikController = newValue;
            },
            enabled: nik.nikController != null ? _disable : false,
            decoration: InputDecoration(
              labelText: "Tulis Nomor NIK",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.done,
          ),
        ),
        SizedBox(
          height: 6,
        ),
        Text(
          "Nama Lengkap Sesuai KTP",
          style: TextStyle(fontSize: 15),
        ),
        SizedBox(
          height: 6,
        ),
        Consumer<SubmitDataConsumen>(
          builder: (_, nama, __) => TextField(
            controller: nama.fullNameController,
            enabled: fullNameController.text != null ? _disable : false,
            decoration: InputDecoration(
              labelText: "Tulis nama lengkap",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
          ),
        ),
        SizedBox(
          height: 6,
        ),
        Text(
          "Nomor HP yang bisa dihibungi",
          style: TextStyle(fontSize: 15),
        ),
        SizedBox(
          height: 6,
        ),
        Consumer<SubmitDataConsumen>(
          builder: (_, phoneNo, __) => TextFormField(
            controller: phoneNo.phoneNoController,
            // onSaved: (value) => phoneNo.setPhoneNoController = value,
            decoration: InputDecoration(
              labelText: "mis. 08098999999",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            keyboardType: TextInputType.number,
          ),
        ),
        SizedBox(
          height: 6,
        ),
        Row(
          children: [
            Consumer<SubmitDataConsumen>(
              builder: (_, checkboxValue, __) => Checkbox(
                value: checkboxValue.checkBox,
                onChanged: (bool value) {
                  checkboxValue.checkBox = value;
                  // checkboxValue.waNoController;
                },
              ),
            ),
            Expanded(
              child: Text("Centang jika Nomor HP sama dengan nomor WA"),
            ),
          ],
        ),
        SizedBox(
          height: 6,
        ),
        Text(
          "Nomor WA",
          style: TextStyle(fontSize: 15),
        ),
        SizedBox(
          height: 6,
        ),
        Consumer<SubmitDataConsumen>(
          builder: (_, waNo, __) => TextFormField(
            controller: waNo.waNoController,
            // onSaved: (value) => waNo.setWaNoController = value,
            enabled: waNo.checkBox,
            decoration: InputDecoration(
              labelText: "mis. 08098999999",
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.done,
          ),
        ),
        SizedBox(
          height: 12,
        ),
        SizedBox(
          height: 6,
        ),
        Center(
          child: TextButton(
            onPressed: () {},
            child: Text(
              "Simpan ke draft",
              style: TextStyle(
                fontSize: 15,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future getImage(ImageSource source) async {
    final _selectedImage =
        await ImagePicker().getImage(source: source, imageQuality: 100);
    if (_selectedImage != null) {
      File cropped = await ImageCropper.cropImage(
        sourcePath: _selectedImage.path,
        //cropStyle: CropStyle.rectangle,
        //aspectRatioPresets: [CropAspectRatioPreset.original],
        //aspectRatio: CropAspectRatio(ratioX: 4, ratioY: 3),
        compressQuality: 100,
        maxHeight: 800,
        maxWidth: 1200,
        compressFormat: ImageCompressFormat.jpg,
        androidUiSettings: AndroidUiSettings(toolbarTitle: "Crop Image"),
      );
      Provider.of<SubmitDataConsumen>(context).imageFile = cropped;
      context.read<SubmitDataConsumen>().UploadFile();
    }
  }

  //

  Widget _dialogPopUpCamera(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Choose an Acction',
        style: TextStyle(color: Colors.blue),
      ),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
              icon: Icon(Icons.camera_alt_sharp),
              iconSize: 70,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TakeImageCamera()),
                );
              }),
          IconButton(
              icon: Icon(Icons.folder_open),
              iconSize: 70,
              onPressed: () {
                FutureBuilder(
                  future: getImage(ImageSource.gallery),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    snapshot.connectionState == ConnectionState.done
                        ? Navigator.pop(context)
                        : CircularProgressIndicator();
                  },
                );
              })
        ],
      ),
    );
  }

  void _handleRadioCustTypeValueChange(int value) {
    _radioCustTypeValue = value;

    if (_radioCustTypeValue == 1) {
    } else {}
  }
}
