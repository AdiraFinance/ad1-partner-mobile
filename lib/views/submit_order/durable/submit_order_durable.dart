import 'package:adira_one_partner/utils/constanta.dart';
import 'package:adira_one_partner/views/submit_order/durable/data_domisili_durable.dart';
import 'package:adira_one_partner/views/submit_order/durable/data_unit_durable.dart';
import 'package:adira_one_partner/views/submit_order/durable/preview_page_durable.dart';
import 'package:flutter/material.dart';
import 'package:pdf/widgets/basic.dart';
import 'package:adira_one_partner/views/submit_order/durable/data_konsumen_durable.dart';

class DataKonsumenDurable extends StatefulWidget {
  @override
  _DataKonsumenState createState() => _DataKonsumenState();
}

class _DataKonsumenState extends State<DataKonsumenDurable> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Pengajuan Kredit',
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        // primarySwatch: kPrimaryColor, //Colors.amber,
        accentColor: Colors.black,
        textTheme: ThemeData.light().textTheme.copyWith(
              headline6: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentStep = 0;
  StepperType stepperType = StepperType.horizontal;
  VoidCallback _onStepContinue, _onStepCancel;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Order Durable"),
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Stepper(
                type: stepperType,
                physics: ScrollPhysics(),
                currentStep: _currentStep,
                onStepTapped: (step) => tapped(step),
                onStepContinue: continued,
                onStepCancel: cancel,
                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  _onStepContinue = onStepContinue;
                  _onStepCancel = onStepCancel;
                  return Container();
                  // SizedBox(
                  //   width: 0,
                  //   height: 0,
                  // );
                },
                steps: <Step>[
                  Step(
                    title: new Text('Data Konsumen'),
                    content: InputDataNasabah(),
                    isActive: _currentStep >= 0,
                    state: _currentStep >= 0
                        ? StepState.complete
                        : StepState.disabled,
                  ),
                  Step(
                    title: new Text('Pembiayaan'),
                    content: DataUnit(),
                    isActive: _currentStep >= 1,
                    state: _currentStep >= 1
                        ? StepState.complete
                        : StepState.disabled,
                  ),
                  Step(
                    title: new Text('Domisili'),
                    content: DataDomisili(),
                    isActive: _currentStep >= 2,
                    state: _currentStep >= 2
                        ? StepState.complete
                        : StepState.disabled,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ButtonBar(
        children: [
          _currentStep == 0
              ? Container(
                  height: 35,
                  width: double.maxFinite,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: kPrimaryColor, onPrimary: Colors.black45),
                    onPressed: () {
                      _onStepContinue();
                    },
                    child: Text("Selanjutnya"),
                  ),
                )
              : _currentStep == 2
                  ? Container(
                      margin: EdgeInsets.all(8),
                      width: double.maxFinite,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: kPrimaryColor,
                                    onPrimary: Colors.black45),
                                onPressed: () {
                                  Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => PreviewPage()));
                                },
                                child: Text("Selanjutnya"),
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: kPrimaryColor,
                                    onPrimary: Colors.black45),
                                onPressed: () {
                                  _onStepCancel();
                                },
                                child: Text("Kembali"),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container(
                      margin: EdgeInsets.all(8),
                      width: double.maxFinite,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: kPrimaryColor,
                                    onPrimary: Colors.black45),
                                onPressed: () {
                                  _onStepContinue();
                                },
                                child: Text("Selanjutnya"),
                              ),
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Container(
                              padding: const EdgeInsets.all(8.0),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    primary: kPrimaryColor,
                                    onPrimary: Colors.black45),
                                onPressed: () {
                                  _onStepCancel();
                                },
                                child: Text("Kembali"),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
        ],
      ),
    );
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    _currentStep < 2 ? setState(() => _currentStep += 1) : null;
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }
}
