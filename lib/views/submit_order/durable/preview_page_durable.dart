import 'package:adira_one_partner/services/submit_order_api_provider.dart';
import 'package:adira_one_partner/utils/constanta.dart';
import 'package:adira_one_partner/utils/size_config.dart';
import 'package:adira_one_partner/views/home.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_domisili.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_unit.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/submit_order.dart';
import 'package:flutter/material.dart';

class PreviewPage extends StatefulWidget {
  @override
  _PreviewPageState createState() => _PreviewPageState();
}

class _PreviewPageState extends State<PreviewPage> {
  Screen size;
  SubmitOrderForm _submitOrderForm;
  var _processSendData = false;

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Preview Order",
          style: TextStyle(
              fontFamily: "Nunito Sans",
              fontSize: 16,
              fontWeight: FontWeight.w700),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            dataKonsumen(),
            SizedBox(
              height: size.hp(2),
            ),
            unitPembiayaan(),
            SizedBox(
              height: size.hp(2),
            ),
            domisiliSurvey(),
            SizedBox(
              height: size.hp(2),
            ),
            voucher(),
            SizedBox(
              height: size.hp(2),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ButtonBar(
        children: [
          Container(
            height: 48,
            width: double.maxFinite,
            margin: EdgeInsets.all(10),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24.0)),
                  primary: kPrimaryColor,
                  onPrimary: Colors.black45),
              onPressed: () {
                // _processData();
              },
              child: Text(
                "Kirim Order",
                style: TextStyle(
                    fontFamily: "Nunito Sans",
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Colors.black),
              ),
            ),
          )
        ],
      ),
    );
  }

  Container dataKonsumen() {
    return Container(
      margin: EdgeInsets.fromLTRB(size.wp(5), 0, size.wp(2), 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Data Konsumen",
                style: TextStyle(
                    fontFamily: "Nunito Sans",
                    fontSize: 16,
                    fontWeight: FontWeight.w700),
              ),
              TextButton(
                child: Text(
                  "Ubah",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff0A66C2)),
                ),
                onPressed: () => Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => DataKonsumen())),
              ),
            ],
          ),
          const Divider(
            height: 1,
            thickness: 2,
            endIndent: 15,
          ),
          SizedBox(height: size.hp(2)),
          Container(
            height: size.hp(10),
            padding: EdgeInsets.only(left: size.wp(5)),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(8),
              // color: Color(0xfffcebb1),
            ),
            child: Row(
              children: [
                Image(image: AssetImage("assets/images/user.png")),
                SizedBox(width: size.wp(5)),
                Text(
                  "Foto KTP",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff92949D)),
                ),
              ],
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "NIK Konsumen",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "3434-4559-4547-1009",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Nama Sesuai KTP",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Jaya Suparno",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Nomor HP yang bisa dihubungi",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "0812-4590-3689",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Nomor WA",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "0812-4590-3689",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
        ],
      ),
    );
  }

  Container domisiliSurvey() {
    return Container(
      margin: EdgeInsets.fromLTRB(size.wp(5), 0, size.wp(2), 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Domisili & Survey",
                style: TextStyle(
                    fontFamily: "Nunito Sans",
                    fontSize: 16,
                    fontWeight: FontWeight.w700),
              ),
              TextButton(
                child: Text(
                  "Ubah",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff0A66C2)),
                ),
                onPressed: () => Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => DataKonsumen())),
              ),
            ],
          ),
          const Divider(
            height: 1,
            thickness: 2,
            endIndent: 15,
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Alamat Lengkap Domisili",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Jl. Radar Baru No 6",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Kelurahan",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Petissari, Kec. Pancoran Mas, Depok, 16431",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Jadwal Survey",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Rabu, 7 April, Pkl 16.30 WIB",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
        ],
      ),
    );
  }

  Container unitPembiayaan() {
    return Container(
      margin: EdgeInsets.fromLTRB(size.wp(5), 0, size.wp(2), 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Unit & Pembiayaan",
                style: TextStyle(
                    fontFamily: "Nunito Sans",
                    fontSize: 16,
                    fontWeight: FontWeight.w700),
              ),
              TextButton(
                child: Text(
                  "Ubah",
                  style: TextStyle(
                      fontFamily: "Nunito Sans",
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xff0A66C2)),
                ),
                onPressed: () => Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => DataKonsumen())),
              ),
            ],
          ),
          const Divider(
            height: 1,
            thickness: 2,
            endIndent: 15,
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Merk Unit",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Samsung",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tipe Unit",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Crystal UHD 50” QLED 4K Smart TV (2020)",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Harga Barang",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Rp9.600.000",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Rate",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "3%",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "DP",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Rp3.000.000",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Tenor Dalam Bulan",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "24",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Cicilan per bulan",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff92949D)),
            ),
          ),
          SizedBox(height: size.hp(1)),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              "Rp850.000",
              style: TextStyle(
                  fontFamily: "Nunito Sans",
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xff12162C)),
            ),
          ),
        ],
      ),
    );
  }

  Container voucher() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.fromLTRB(size.wp(5), size.hp(5), size.wp(2), 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: size.hp(10),
            padding: EdgeInsets.only(left: size.wp(5)),
            margin: EdgeInsets.only(right: size.wp(5)),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(8),
              color: Color(0xffF6F6F6),
            ),
            child: Row(
              children: [
                Image(image: AssetImage("assets/images/notif.png")),
                SizedBox(width: size.wp(2)),
                Expanded(
                  child: Text(
                    "Pastikan data sudah benar sebelum mengirim order kredit",
                    style: TextStyle(
                        fontFamily: "Nunito Sans",
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff4F5262)),
                  ),
                ),
              ],
            ),
          ),
          const Divider(
            height: 1,
            thickness: 4,
            endIndent: 15,
          ),
          SizedBox(height: size.hp(5)),
          Text(
            "Masukkan Kode Voucher (opsional)",
            style: TextStyle(
                fontFamily: "Nunito Sans",
                fontSize: 12,
                fontWeight: FontWeight.w700),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            height: size.hp(10),
            padding: EdgeInsets.only(left: size.wp(5)),
            margin: EdgeInsets.only(right: size.wp(5)),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(8),
              color: Color(0xffF6F9FE),
            ),
            child: Row(
              children: [
                Image(image: AssetImage("assets/images/ticket.png")),
                SizedBox(width: size.wp(2)),
                Expanded(
                  child: TextField(
                    decoration: new InputDecoration(
                      labelText: 'Ketik Kode Voucher disini',
                      labelStyle: TextStyle(fontFamily: "NunitoSans"),
                      // border: OutlineInputBorder(
                      //     borderRadius: BorderRadius.circular(8)),
                    ),
                    style: TextStyle(
                        fontFamily: "Nunito Sans",
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Color(0xff92949D)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _processData() async {
    Navigator.pop(context);
    setState(() {
      _processSendData = true;
    });
    var _body = {
      "credOrderNo": "cobaDiki",
      "createdBy": "10065901",
      "createdDate": "2021-02-19T09:41:46.253Z",
      "credApplFlagSource": "01",
      "credApplNoUnit": "000000001",
      "credApplSourceCode": "01",
      "credBirthDate": "1995-02-19T09:41:46.253Z",
      "credBirthPlace": "indonesia",
      "credBrId": "0341",
      "credCompPicName": "string",
      "credCustType": "001",
      "credDateSurvey": "2021-02-19T09:41:46.253Z",
      "credDealNotes": "NOTES",
      "credEconomySector": "0001",
      "credFinProduct": "0",
      "credFinType": "0",
      "credFlagAddr": "1",
      "credFotoKtpNpwp": "5",
      "credFotoStnk": "15",
      "credIdDate": "2021-02-19T09:41:46.253Z",
      "credIdNo": "0000111144447777",
      "credInstAmt": "0",
      "credInsuranceType": "1",
      "credJenisUsaha": "0001",
      "credKabKotaId": "0001",
      "credKecamatanId": "00001",
      "credKelurahanId": "00001",
      "credKtpAddr": "indonesia",
      "credMaidenName": "ibuk",
      "credMarriageStatus": "001",
      "credMktNik": "10065901",
      "credMobilePhNo": "08121212121",
      "credModelDetail": "02",
      "credName": "namenya",
      "credNameOnBpkb": "name bpkb",
      "credNetDp": "0",
      "credNpwpDate": "2021-02-19T09:41:46.253Z",
      "credObbrCode": "001",
      "credObjtCode": "001",
      "credObjtFotoSisi4": "5",
      "credObjtPrice": "0",
      "credObjtYear": "0001",
      "credObmoCode": "001",
      "credObtyCode": "001",
      "credOccupation": "001",
      "credPrincipalAmt": "0",
      "credProvinceId": "01",
      "credRtRwKtp": "string",
      "credSex": "01",
      "credSidName": "string",
      "credSurveyAddr": "string",
      "credSvyKabKotaId": "0001",
      "credSvyKecamatanId": "00001",
      "credSvyKelurahanId": "00004",
      "credSvyProvinceId": "01",
      "credSvyRtRw": "string",
      "credSvyType": "00005",
      "credSvyZipCode": "43359",
      "credTelpNo": "string",
      "credTimeSurvey": "19:19",
      "credTop": "0",
      "credTypeBarang": "string",
      "credVoucherCode": "string",
      "credZipCode": "43359"
    };
    var _result = await _submitOrderForm.submitOrder(_body);
    if (_result['status']) {
      setState(() {
        _processSendData = false;
      });
      Navigator.pop(context);
      _showDialogSuccess(_result['data'][0]);
      // _validatesubmit = true;
      // _isProcessCheckVoucher = false;
    } else {
      setState(() {
        _processSendData = false;
      });
      Navigator.pop(context);
      _showDialogError(_result['data']['error_description']);
      // _validatesubmit = false;
    }
  }

  _showDialogSuccess(Map message) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Success", style: TextStyle(fontFamily: "NunitoSans")),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(child: Text("Order No"), flex: 3),
                    Expanded(child: Text(" : "), flex: 0),
                    Expanded(
                        child: Text(message['OrderNo'] != null
                            ? message['OrderNo']
                            : ""),
                        flex: 7),
                  ],
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomePage(
                                  dif: 1,
                                )),
                        (Route<dynamic> route) => false);
                  },
                  child: Text("Close",
                      style: TextStyle(
                          fontFamily: "NunitoSans", color: Colors.yellow))),
            ],
          );
        });
  }

  _showDialogError(String message) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(message, style: TextStyle(fontFamily: "NunitoSans")),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    // setState(() {
                    //   _pemilihanCaraSurvey = _listPemilihanCaraSurvey[0];
                    // });
                  },
                  child: Text("Close",
                      style: TextStyle(
                          fontFamily: "NunitoSans", color: Colors.yellow))),
            ],
          );
        });
  }
}
