import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTextFormField extends StatelessWidget {
  //
  AppTextFormField(
      {this.controller,
      this.hintText,
      this.helpText,
      this.prefixIcon,
      this.suffixIcon,
      this.isPassword,
      this.enabled,
      this.readOnly,
      this.borderColor,
      this.onSaved,
      this.keyboardType,
      this.textInputAction,
      this.onTap,
      this.validator,
      this.labelText,
      this.inputFormatter});

  final TextEditingController controller;
  final String hintText;
  final String helpText;
  final IconData prefixIcon;
  final IconData suffixIcon;
  final bool isPassword;
  final bool enabled;
  final bool readOnly;
  final Color borderColor;
  final Function onSaved;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final Function onTap;
  final Function validator;
  final String labelText;
  final List<TextInputFormatter> inputFormatter;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        inputFormatters: null == inputFormatter ? [] : inputFormatter,
        style: TextStyle(
            fontFamily: "Nunito Sans",
            fontSize: 14,
            fontWeight: FontWeight.w400,
            color: Colors.black),
        controller: controller,
        readOnly: null == readOnly ? false : true,
        obscureText: null == isPassword ? false : true,
        keyboardType: null == keyboardType ? TextInputType.text : keyboardType,
        textInputAction:
            null == textInputAction ? TextInputAction.done : textInputAction,
        decoration: InputDecoration(
          filled: true,
          fillColor: Color(0xFFF6F9FE),
          hintStyle: TextStyle(color: Colors.black),
          labelStyle: TextStyle(color: Colors.black),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color(0xFFF6F9FE),
              width: 2.0,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.blueGrey[100],
              width: 1.0,
            ),
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: null == borderColor ? Colors.grey : borderColor,
              width: 1.0,
            ),
          ),
          hintText: null == hintText ? '' : hintText,
          helperText: null == helpText ? '' : helpText,
          prefixIcon: null == prefixIcon ? null : Icon(prefixIcon),
          suffix: null == suffixIcon ? null : Icon(suffixIcon),
          enabled: null == enabled ? true : enabled,
          labelText: null == labelText ? '' : labelText,
        ),
        onSaved: null == onSaved ? null : onSaved,
        onTap: null == onTap ? null : onTap,
        validator: null == validator ? null : validator,
      ),
    );
  }
}
