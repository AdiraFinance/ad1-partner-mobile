class DealerInfo {
  final String dealName;
  final String address;
  final String phoneNo;

  DealerInfo(this.dealName, this.address, this.phoneNo);
}
