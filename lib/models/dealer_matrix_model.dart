class DealerMatrix {
  final String gradeNmcy;
  final String effNmcy;
  final String gradeUmcy;
  final String effUmcy;
  final String gradeNcar;
  final String effNcar;
  final String gradeUcar;
  final String effUcar;
  final String fullName;

  DealerMatrix(
      this.gradeNmcy,
      this.effNmcy,
      this.gradeUmcy,
      this.effUmcy,
      this.gradeNcar,
      this.effNcar,
      this.gradeUcar,
      this.effUcar,
      this.fullName);
}
