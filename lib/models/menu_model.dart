class MenuModel {
  final int menuProfile;
  final int menuSubmitOrder;
  final int menuTrackingOrder;
  final int menuDraft;
  final int menuChat;
  final int menuReschedule;

  MenuModel(this.menuProfile, this.menuSubmitOrder, this.menuTrackingOrder,
      this.menuDraft, this.menuChat, this.menuReschedule);
}
