import 'package:adira_one_partner/services/submit_data_konsumen_notifier.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/submit_order.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:provider/provider.dart';

import 'ocr_ktp.dart';

class FotoPreview extends StatefulWidget {
  // final String nik;
  // final File image;
  // FotoPreview({this.nik, this.image});

  @override
  _FotoPreviewState createState() => _FotoPreviewState();
}

class _FotoPreviewState extends State<FotoPreview> {
  File ximage;

  Future<File> uploadFile(File _image) async {
    Response<String> response;
    var dio = Dio();
    String path = "https://idpgwuat.adiraku.co.id/ocr/v1/ktp/results";
    String fileName = _image.path.split("/").last;
    try {
      FormData formData = new FormData.fromMap({
        "requestId": "5922441233443",
        "file": await MultipartFile.fromFile(_image.path,
            filename: fileName, contentType: MediaType('image', 'jpg')),
        "type": "image/jpg"
      });
      response = await dio.post(path, data: formData);
      // _ocrKtp;
      OcrKtp _ocrKtp = ocrKtpFromJson(response.data);
      setState(() {
        ximage = _image;
      });
      print(response.data);
      return ximage;
    } catch (e) {
      print(e.toString());
    } finally {
      dio.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final nikController = TextEditingController();

    // if (widget.nik != null) nikController.text = widget.nik;

    return Scaffold(
      appBar: AppBar(
        title: Text('Preview KTP'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Consumer<SubmitDataConsumen>(
              builder: (_, camera, __) => FutureBuilder(
                future: camera.UploadFile(),
                builder: (BuildContext context, AsyncSnapshot snapshot) =>
                    snapshot.connectionState == ConnectionState.done
                        ? SizedBox(
                            child: Image.file(
                              camera.imageFile,
                              fit: BoxFit.cover,
                            ),
                          )
                        : AspectRatio(
                            aspectRatio: 4 / 3,
                            child: Container(
                                padding: EdgeInsets.all(8),
                                alignment: Alignment.center,
                                child: CircularProgressIndicator()),
                          ),
              ),
            ),
            SizedBox(
              height: mediaQuery.size.height * 0.02,
            ),
            Text('Nomor Induk Kependudukan (NIK)'),
            SizedBox(
              height: mediaQuery.size.height * 0.01,
            ),
            Consumer<SubmitDataConsumen>(
              builder: (_, nik, __) => TextField(
                controller: nik.nikController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                readOnly: true,
              ),
            ),
            SizedBox(
              height: mediaQuery.size.height * 0.02,
            ),
            Text('Nama Lengkap Sesuai KTP'),
            SizedBox(
              height: mediaQuery.size.height * 0.01,
            ),
            Consumer<SubmitDataConsumen>(
              builder: (_, nama, __) => TextField(
                controller: nama.fullNameController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                readOnly: true,
              ),
            ),
            SizedBox(
              height: mediaQuery.size.height * 0.01,
            ),
            Text(
              'Silahkan periksa apakah data sudah benar',
              style: TextStyle(color: Colors.grey[400]),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        padding: EdgeInsets.all(8),
        width: mediaQuery.size.width,
        child: ElevatedButton(
          onPressed: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => DataKonsumen(),
            ),
          ),
          child: Text('Gunakan Ini'),
          style: ButtonStyle(
            shape: MaterialStateProperty.all<OutlinedBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            )),
          ),
        ),
      ),
      bottomNavigationBar: ButtonBar(
        alignment: MainAxisAlignment.center,
        children: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              'Unggah Ulang',
              style: TextStyle(color: Colors.blue[300]),
            ),
          ),
        ],
      ),
    );
  }
}
