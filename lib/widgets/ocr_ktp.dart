// To parse this JSON data, do
//
//     final ocrKtp = ocrKtpFromJson(jsonString);

import 'dart:convert';

OcrKtp ocrKtpFromJson(String str) => OcrKtp.fromJson(json.decode(str));

String ocrKtpToJson(OcrKtp data) => json.encode(data.toJson());

class OcrKtp {
  OcrKtp({
    this.commonResponse,
    this.ktpData,
  });

  CommonResponse commonResponse;
  KtpData ktpData;

  factory OcrKtp.fromJson(Map<String, dynamic> json) => OcrKtp(
        commonResponse: CommonResponse.fromJson(json["commonResponse"]),
        ktpData: KtpData.fromJson(json["ktpData"]),
      );

  Map<String, dynamic> toJson() => {
        "commonResponse": commonResponse.toJson(),
        "ktpData": ktpData.toJson(),
      };
}

class CommonResponse {
  CommonResponse({
    this.responseCode,
    this.type,
    this.message,
  });

  String responseCode;
  String type;
  String message;

  factory CommonResponse.fromJson(Map<String, dynamic> json) => CommonResponse(
        responseCode: json["responseCode"],
        type: json["type"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "type": type,
        "message": message,
      };
}

class KtpData {
  KtpData({
    this.provinsi,
    this.kota,
    this.nik,
    this.nama,
    this.tempatLahir,
    this.tanggalLahir,
    this.jenisKelamin,
    this.alamat,
    this.rt,
    this.rw,
    this.kelDesa,
    this.kecamatan,
    this.agama,
    this.statusPerkawinan,
    this.pekerjaan,
    this.tanggalPembuatan,
    this.rawData,
  });

  Data provinsi;
  Data kota;
  Data nik;
  Data nama;
  Data tempatLahir;
  Data tanggalLahir;
  Data jenisKelamin;
  Data alamat;
  Data rt;
  Data rw;
  Data kelDesa;
  Data kecamatan;
  Data agama;
  Data statusPerkawinan;
  Data pekerjaan;
  dynamic tanggalPembuatan;
  List<String> rawData;

  factory KtpData.fromJson(Map<String, dynamic> json) => KtpData(
        provinsi: json["provinsi"] == null
            ? json["provinsi"]
            : Data.fromJson(json["provinsi"]),
        kota: json["kota"] == null ? json["kota"] : Data.fromJson(json["kota"]),
        nik: json["nik"] == null ? json["nik"] : Data.fromJson(json["nik"]),
        nama: json["nama"] == null ? json["nama"] : Data.fromJson(json["nama"]),
        tempatLahir: json["tempatLahir"] == null
            ? json["tempatLahir"]
            : Data.fromJson(json["tempatLahir"]),
        tanggalLahir: json["tanggalLahir"] == null
            ? json["tanggalLahir"]
            : Data.fromJson(json["tanggalLahir"]),
        jenisKelamin: json["jenisKelamin"] == null
            ? json["jenisKelamin"]
            : Data.fromJson(json["jenisKelamin"]),
        alamat: json["alamat"] == null
            ? json["alamat"]
            : Data.fromJson(json["alamat"]),
        rt: json["rt"] == null ? json["rt"] : Data.fromJson(json["rt"]),
        rw: json["rw"] == null ? json["rw"] : Data.fromJson(json["rw"]),
        kelDesa: json["kelDesa"] == null
            ? json["kelDesa"]
            : Data.fromJson(json["kelDesa"]),
        kecamatan: json["kecamatan"] == null
            ? json["kecamatan"]
            : Data.fromJson(json["kecamatan"]),
        agama: json["agama"] == null
            ? json["agama"]
            : Data.fromJson(json["agama"]),
        statusPerkawinan: json["statusPerkawinan"] == null
            ? json["statusPerkawinan"]
            : Data.fromJson(json["statusPerkawinan"]),
        pekerjaan: json["pekerjaan"] == null
            ? json["pekerjaan"]
            : Data.fromJson(json["pekerjaan"]),
        tanggalPembuatan: json["tanggalPembuatan"],
        rawData: json["rawData"] == null
            ? null
            : List<String>.from(json["rawData"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "provinsi": provinsi.toJson(),
        "kota": kota.toJson(),
        "nik": nik.toJson(),
        "nama": nama.toJson(),
        "tempatLahir": tempatLahir.toJson(),
        "tanggalLahir": tanggalLahir.toJson(),
        "jenisKelamin": jenisKelamin.toJson(),
        "alamat": alamat.toJson(),
        "rt": rt.toJson(),
        "rw": rw.toJson(),
        "kelDesa": kelDesa.toJson(),
        "kecamatan": kecamatan.toJson(),
        "agama": agama.toJson(),
        "statusPerkawinan": statusPerkawinan.toJson(),
        "pekerjaan": pekerjaan.toJson(),
        "tanggalPembuatan": tanggalPembuatan,
        "rawData": List<dynamic>.from(rawData.map((x) => x)),
      };
}

class Data {
  Data({
    this.value,
    this.confidenceLevel,
  });

  String value;
  String confidenceLevel;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        value: json["value"],
        confidenceLevel: json["confidenceLevel"],
      );

  Map<String, dynamic> toJson() => {
        "value": value,
        "confidenceLevel": confidenceLevel,
      };
}
