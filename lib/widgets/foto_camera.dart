import 'dart:io';

import 'package:adira_one_partner/services/submit_data_konsumen_notifier.dart';
import 'package:adira_one_partner/widgets/foto_preview.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

class TakeImageCamera extends StatefulWidget {
  @override
  _TakeImageCameraState createState() => _TakeImageCameraState();
}

class _TakeImageCameraState extends State<TakeImageCamera> {
  CameraController controller;
  File ximage;
  String nik;
  File newFile;

  Future<void> initializedCamera() async {
    var cameras = await availableCameras();
    controller = CameraController(cameras[0], ResolutionPreset.high);
    await controller.initialize();

    print('Camera Height ${controller.value.previewSize.height}');
    print('Camera Width ${controller.value.previewSize.width}');
    print(controller.value.previewSize.aspectRatio);
    print(controller.value.aspectRatio);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  takePicture(double width, double height) async {
    Directory root = await getTemporaryDirectory();
    String directoryPath = '${root.path}/Guided_Camera';
    await Directory(directoryPath).create(recursive: true);
    // String filePath = '$directoryPath/${DateTime.now()}.jpg';

    try {
      await controller.takePicture().then((camera) async {
        context
            .read<SubmitDataConsumen>()
            .cropSquare(camera.path, camera.path, false, width, height);

        //cropSquare(value.path, value.path, false, width, height);
      });
    } catch (e) {
      return Consumer<SubmitDataConsumen>(
          builder: (_, value, __) => value.imageFile);
    }
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final appBar = AppBar(
      title: Text('Sesuaikan Foto'),
    );
    print('Handphone height ${mediaQuery.size.height}');
    print('Handphone width ${mediaQuery.size.width}');
    print(mediaQuery.size.aspectRatio);

    return Scaffold(
      appBar: appBar,
      backgroundColor: Colors.white,
      body: FutureBuilder(
        future: initializedCamera(),
        builder: (_, snapshot) =>
            (snapshot.connectionState == ConnectionState.done)
                ? Stack(
                    children: [
                      //  2 / controller.value.previewSize.aspectRatio,

                      SizedBox(
                        width: mediaQuery.size.width,
                        height: (mediaQuery.size.height -
                                appBar.preferredSize.height -
                                mediaQuery.padding.top) *
                            controller.value.previewSize.aspectRatio,
                        child: CameraPreview(controller),
                        // ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            height: (mediaQuery.size.height -
                                    appBar.preferredSize.height -
                                    mediaQuery.padding.top) *
                                0.4,
                            width: mediaQuery.size.width,
                            //  2 / controller.value.previewSize.aspectRatio,
                            child: Image.asset(
                              'assets/images/ktp.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                          Container(
                            color: Colors.white,
                            height: (mediaQuery.size.height -
                                    appBar.preferredSize.height -
                                    mediaQuery.padding.top) *
                                0.6,
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.all(20),
                                  child: Text(
                                    'Untuk mendapatkan hasil verifikasi yang baik, mohon perhatikan informasi berikut:',
                                    style: TextStyle(fontSize: 20),
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Icon(Icons.check),
                                      ),
                                      Expanded(
                                        child: Text(
                                            "KTP harus KTP asli, bukan hasil pindai, copy, atau versi unggahan"),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Icon(Icons.check),
                                      ),
                                      Expanded(
                                        child: Text(
                                            "Pastikan posisi rata sesuai dengan bingkai yang disediakan"),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  )
                : Center(
                    child: SizedBox(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator(),
                    ),
                  ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          if (!controller.value.isTakingPicture) {
            await takePicture(
                (mediaQuery.size.height) / controller.value.aspectRatio,
                controller.value.previewSize.height);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => FotoPreview(),
              ),
            );
          }
        },
        child: Icon(Icons.camera_alt_rounded),
      ),
    );
  }
}
