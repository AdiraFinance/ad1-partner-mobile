import 'dart:convert';
import 'dart:io';
import 'package:adira_one_partner/db_helper/database_helper.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' show Client;
import 'package:http/io_client.dart';

class OrderApiProvider {
  Client client = Client();
  DbHelper _dbHelper = DbHelper();

  Future<Map> SearchKecamatan(String param) async {
    try {
      final _response = await client.get(
          "https://af-extwsuat.adira.co.id/Ad1gateAPI/api/Submit/GetKecamatan?search=" +
              param);
      final _data = jsonDecode(_response.body);
      print(_data);
      if (_data['Message'] == "Success") {
        return _data;
      } else {
        throw Exception('Failed to load jobs from API');
      }
    } catch (e) {
      print(e.toString());
      var _result = {"status": false, "message": e.toString()};
      return _result;
    }
  }

  Future<Map> SubmitOrder(Map param) async {
    try {
      String body = json.encode(param);

      var dio = Dio();
      final _response = await dio.post(
          'http://hoitbad497.adira.co.id:1020/leadengine/order',
          data: body);
      print(_response);
      final _data = jsonDecode(_response.toString());
      return _data;
    } catch (e) {
      print(e.toString());
      var _result = {"status": false, "message": e.toString()};
      return _result;
    }
  }

  Future<String> GetOrderNo() async {
    try {
      var dio = Dio();
      final _response = await dio.post(
          'http://10.81.3.137:1234/api/service/generate/orderid',
          data: {'columnName': "USA"});
      return _response.data['orderNo'];
    } catch (e) {
      print(e.toString());
      var _result = {"status": false, "message": e.toString()};
      return e.toString();
    }
  }
}
