import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:adira_one_partner/views/home.dart';
import 'package:adira_one_partner/utils/constanta.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

// import '../main.dart';

class LoginChangeNotifier with ChangeNotifier {
  TextEditingController _controllerEmail = TextEditingController()
    ..text = 'ronny.suhermawan@adira.co.id';
  TextEditingController _controllerPassword = TextEditingController()
    ..text = 'adira';

  bool _secureText = true;
  bool _autoValidate = false;
  bool _loginProcess = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  // var storage = FlutterSecureStorage();

  TextEditingController get controllerEmail => _controllerEmail;
  TextEditingController get controllerPassword => _controllerPassword;
  bool get secureText => _secureText;
  bool get loginProcess => _loginProcess;

  set loginProcess(bool value) {
    _loginProcess = value;
    notifyListeners();
  }

  set secureText(bool value) {
    this._secureText = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
  // GlobalKey<ScaffoldMessengerState> get scaffoldMessengerKey => _scaffoldMessengerKey;
  GlobalKey<FormState> get key => _key;

  showHidePass() {
    _secureText = !_secureText;
    notifyListeners();
  }

  void check(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    if (_controllerEmail.text != "" && _controllerPassword.text != "") {
      loginProcess = true;
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      final _http = IOClient(ioc);
      try {
        final _response = await _http.post(
          "http://10.81.3.61:99/CommonsWebAPI/api/PublicAccess/Authenticate",
          body: {
            "login": _controllerEmail.text,
            "password": _controllerPassword.text
          },
          // headers: {
          //   "Content-Type": "application/json"
          // }
        ).timeout(Duration(seconds: 50));
        final _result = jsonDecode(_response.body);
        print("cek login ${_response.statusCode}");
        if (_result['Message'] == 'OK') {
          // getInfoPartner(context);
          // getMatrixPartner(context);
          loginProcess = false;
          _preferences.setString("userLogin", _controllerEmail.text);
          print(_preferences.getString("userLogin"));
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage()));
        } else {
          showSnackBar(_result['Message']);
        }
      } on TimeoutException catch (_) {
        loginProcess = false;
        showSnackBar("Timeout connection");
      } catch (e) {
        print(e);
        loginProcess = false;
        // showSnackBar(e.toString());
      }
      loginProcess = false;
    } else {
      autoValidate = true;
    }
  }

  void getInfoPartner(BuildContext context) async {
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    try {
      final _response = await _http
          .post("10.81.3.137:4567/api/ad1-service-aol/deal-detail", body: {
        "SZDEAL": "049225"
      }, headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      print(_result);
      // if (_result['Message'] == 'OK') {
      //     loginProcess = false;
      //     Navigator.of(context).pushReplacement(
      //         MaterialPageRoute(builder: (context) => HomePage()));
      //   } else {
      //     showSnackBar(_result['Message']);
      //   }
    } on TimeoutException catch (_) {
      showSnackBar("Timeout connection");
    } catch (e) {
      print(e);
    }
  }

  void getMatrixPartner(BuildContext context) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    try {
      final _response = await _http
          .post("10.81.3.137:4567/api/ad1-service-aol/deal-matrix", body: {
        "SZDIF": "049225"
      }, headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }).timeout(Duration(seconds: 30));
      final _result = jsonDecode(_response.body);
      print(_result);
      _preferences.setString("gradeNmcy", "${_result['gradeNmcy']}");
      _preferences.setString("effNmcy", "${_result['effNmcy']}");
      _preferences.setString("gradeUmcy", "${_result['gradeUmcy']}");
      _preferences.setString("effUmcy", "${_result['effUmcy']}");
      _preferences.setString("gradeNcar", "${_result['gradeNcar']}");
      _preferences.setString("effNcar", "${_result['effNcar']}");
      _preferences.setString("gradeUcar", "${_result['gradeUcar']}");
      _preferences.setString("effUcar", "${_result['effUcar']}");
      _preferences.setString("fullName", "${_result['fullName']}");
      print(_preferences.getString("fullName"));
      // if (_result['Message'] == 'OK') {
      //     loginProcess = false;
      //     Navigator.of(context).pushReplacement(
      //         MaterialPageRoute(builder: (context) => HomePage()));
      //   } else {
      //     showSnackBar(_result['Message']);
      //   }
    } on TimeoutException catch (_) {
      showSnackBar("Timeout connection");
    } catch (e) {
      print(e);
    }
  }

  showSnackBar(String text) {
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,
        backgroundColor: snackbarColor,
        duration: Duration(seconds: 2)));
  }
}
