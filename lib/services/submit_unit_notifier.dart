import 'package:flutter/cupertino.dart';

class SubmitUnitNotifier extends ChangeNotifier {
  final _controllerModelKendaraan = new TextEditingController();
  final _controllerTipeKendaraan = new TextEditingController();
  final _controllerMerkKendaraan = new TextEditingController();
  TextEditingController _controllerTipeKendaraanAll =
      new TextEditingController();
  TextEditingController _controllerOtr = new TextEditingController()
    ..text = "0";
  TextEditingController _controllerTenor = new TextEditingController();
  TextEditingController _controllerDp = new TextEditingController()..text = "0";
  TextEditingController _controllerCicilan = new TextEditingController()
    ..text = "0";

  TextEditingController get controllerTipeKendaraanAll =>
      _controllerTipeKendaraanAll;

  set controllerTipeKendaraanAll(TextEditingController value) {
    this._controllerTipeKendaraanAll = value;
    notifyListeners();
  }

  TextEditingController get controllerOtr => _controllerOtr;

  set controllerOtr(TextEditingController value) {
    this._controllerOtr = value;
    notifyListeners();
  }

  TextEditingController get controllerTenor => _controllerTenor;

  set controllerTenor(TextEditingController value) {
    this._controllerTenor = value;
    notifyListeners();
  }

  TextEditingController get controllerDp => _controllerDp;

  set controllerDp(TextEditingController value) {
    this._controllerDp = value;
    notifyListeners();
  }

  TextEditingController get controllerCicilan => _controllerCicilan;

  set controllerCicilan(TextEditingController value) {
    this._controllerCicilan = value;
    notifyListeners();
  }

  var idMerkKendaraan;
  var idTypeKendaraan;
  var idModelKendaraan;

  setValueModel(Map _modelKendaraan) {
    print("data model kendaraan $_modelKendaraan");
    idMerkKendaraan = _modelKendaraan['OBBR_CODE'];
    idTypeKendaraan = _modelKendaraan['OBTY_CODE'];
    idModelKendaraan = _modelKendaraan['OBMO_CODE'];
    _controllerModelKendaraan.text = _modelKendaraan['OBBR_DESC'];
    _controllerTipeKendaraan.text = _modelKendaraan['OBTY_DESC'];
    _controllerMerkKendaraan.text = _modelKendaraan['OBMO_DESC'];
    _controllerTipeKendaraanAll.text =
        _modelKendaraan['OBBR_DESC'].toString().trim() +
            ', ' +
            _modelKendaraan['OBTY_DESC'].toString().trim() +
            ', ' +
            _modelKendaraan['OBMO_DESC'].toString().trim();
  }

  void clearDataUnit() {
    this._controllerTipeKendaraan.clear();
    this._controllerTipeKendaraanAll.clear();
    this._controllerCicilan.text = "0";
    this._controllerDp.text = "0";
    this._controllerMerkKendaraan.clear();
    this._controllerModelKendaraan.clear();
    this._controllerOtr.text = "0";
    this._controllerTenor.clear();
    this.idMerkKendaraan = null;
    this.idModelKendaraan = null;
    this.idTypeKendaraan = null;
  }
}
