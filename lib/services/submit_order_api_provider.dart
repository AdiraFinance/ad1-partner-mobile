import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:adira_one_partner/utils/url.dart';
import 'package:adira_one_partner/db_helper/database_helper.dart';
import 'package:http/http.dart' show Client;
import 'package:http/io_client.dart';

class SubmitOrderForm {
  DbHelper _dbHelper = DbHelper();

  Future<Map> submitOrder(var body) async {
    Client _client = Client();
    //List _resulTokenFromSQLIte = await _dbHelper.getToken();
    //print("${_resulTokenFromSQLIte[0]['token']}");
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
//    var _body = jsonEncode(body);
    try {
//      final _response = await http.post(
//          "${BaseUrl.url}Submit/SubmitOrderAd1gate", headers: {"Content-Type":"application/json"},
//          body: _body
//      );
      print("submit to LE");
      final _response = await _client
          .post("http://10.61.27.105:1020/leadengine/order", body: body
              //     {
              //   "credSvyRtRw": "string",
              // "credApplFlagSource": "01",
              // "credIdNo": "0000111144447777",
              // "credSex": "01",
              // "credName": "coba",
              // "credCompPicName": "string",
              // "credRtRwKtp": "string",
              // "credProvinceId": "01",
              // "credNpwpDate": 1613727706253,
              // "credSidName": "string",
              // "credBirthDate": 793186906253,
              // "credObmoCode": "001",
              // "credFotoStnk": "5",
              // "credKabKotaId": "0001",
              // "credObjtYear": "0001",
              // "credTop": 0,
              // "credIdDate": 1613727706253,
              // "credVoucherCode": "string",
              // "credSvyType": "00005",
              // "credTimeSurvey": "19:19",
              // "credSurveyAddr": "string",
              // "credDealNotes": "NOTES",
              // "credTelpNo": "string",
              // "credCustType": "001",
              // "credKecamatanId": "00001",
              // "credFlagAddr": "1",
              // "credFotoKtpNpwp": "5",
              // "credOccupation": "001",
              // "credKelurahanId": "00001",
              // "credMobilePhNo": "08121212121",
              // "credMktNik": "10991430",
              // "credBirthPlace": "infonedia",
              // "credInstAmt": 0,
              // "credNetDp": 0,
              // "credFinProduct": "0",
              // "credObjtCode": "001",
              // "credTypeBarang": "string",
              // "credSvyZipCode": "43359",
              // "credJenisUsaha": "0001",
              // "credObtyCode": "001",
              // "credObjtPrice": 0,
              // "credKtpAddr": "indonesia",
              // "credZipCode": "43359",
              // "credModelDetail": "02",
              // "credNameOnBpkb": "name bpkb",
              // "credMaidenName": "ibuk",
              // "credObbrCode": "001",
              // "credDateSurvey": 1613727706253,
              // "credBrId": "0321",
              // "credFinType": 0,
              // "credPrincipalAmt": 0,
              // "credSvyKabKotaId": "0001",
              // "credApplSourceCode": "01",
              // "credEconomySector": "0001",
              // "credTimeMinuteSurvey": "19:19",
              // "credSvyProvinceId": "01",
              // "credObjtFotoSisi42": "42",
              // "credSvyKelurahanId": "00004",
              // "credObjtFotoSisi4": "4",
              // "credMarriageStatus": "001",
              // "credObjtFotoSisi43": "43",
              // "credObjtFotoSisi44": "44",
              // "credInsuranceType": "1",
              // "credSvyKecamatanId": "00001",
              // "credOrderNo": "",
              // "createdBy": "10991431"
              // }
              );
      print("cek status${_response.statusCode}");
      print(_response.body);
      final _data = jsonDecode(_response.body);
      print("cek response data $_data");
      if (_response.statusCode == 200) {
        var _result;
        if (_data['Status'] == 0) {
          _result = {
            "status": true,
            "message": _data['message'],
            "data": _data['data']
          };
        } else {
          var _dataError = {"error_description": _data['message']};
          _result = {"status": false, "data": _dataError};
        }
        return _result;
      } else {
        var _result = {"status": false, "data": _data};
        return _result;
      }
    }
//    on TimeoutException catch(_){
//      var _result = {"status":false,"message":"Connection Timeout"};
//      return _result;
//    }
    catch (e) {
      var _dataError = {"error_description": e.toString()};
      var _result = {"status": false, "data": _dataError};
      return _result;
    }
  }
}
