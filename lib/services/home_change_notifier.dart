import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:adira_one_partner/models/dealer_info_model.dart';
import 'package:adira_one_partner/models/dealer_matrix_model.dart';
import 'package:adira_one_partner/utils/constanta.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeChangeNotifier with ChangeNotifier {
  List<DealerMatrix> _dealerMatrix = [];
  List<DealerInfo> _dealerInfo = [];
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _key = GlobalKey<FormState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
  GlobalKey<FormState> get key => _key;
  List<DealerMatrix> get dealerMatrix => _dealerMatrix;
  List<DealerInfo> get dealerInfo => _dealerInfo;
  String grade, portfolio, fullName;

  bool _loadData = false;
  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  void getInfoPartner(BuildContext context) async {
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);
    try {
      final _response = await _http.post(
          "http://10.81.3.137:4567/api/ad1-service-aol/deal-detail",
          body: {
            "SZDEAL": "049225"
          },
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }).timeout(Duration(seconds: 10));
      final _result = jsonDecode(_response.body);
      print(_result);
      print(_result.toString() != '[]');
      if (_result.toString() != '[]') {
        this._dealerInfo.add(DealerInfo(
            _result['dealName'], _result['address'], _result['phoneNo']));
      }
      // if (_result['Message'] == 'OK') {
      //     loginProcess = false;
      //     Navigator.of(context).pushReplacement(
      //         MaterialPageRoute(builder: (context) => HomePage()));
      //   } else {
      //     showSnackBar(_result['Message']);
      //   }
    } on TimeoutException catch (_) {
      showSnackBar("Timeout connection");
    } catch (e) {
      print(e);
    }
    this._loadData = false;
  }

  void getMatrixPartner(BuildContext context) async {
    this._loadData = true;
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final _http = IOClient(ioc);

    fullName = _preferences.getString("userLogin");
    print(fullName);
    try {
      final _response = await _http.post(
          "http://10.81.3.137:4567/api/ad1-service-aol/deal-matrix",
          body: {
            "SZDIF": "049225"
          },
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          }).timeout(Duration(seconds: 20));
      final _result = jsonDecode(_response.body);
      print(_result);
      this._dealerMatrix.add(DealerMatrix(
          _result['gradeNmcy'],
          _result['effNmcy'],
          _result['gradeUmcy'],
          _result['effUmcy'],
          _result['gradeNcar'],
          _result['effNcar'],
          _result['gradeUcar'],
          _result['effUcar'],
          _result['fullName']));
      // _preferences.setString("gradeNmcy", "${_result['gradeNmcy']}");
      // _preferences.setString("effNmcy", "${_result['effNmcy']}");
      // _preferences.setString("gradeUmcy", "${_result['gradeUmcy']}");
      // _preferences.setString("effUmcy", "${_result['effUmcy']}");
      // _preferences.setString("gradeNcar", "${_result['gradeNcar']}");
      // _preferences.setString("effNcar", "${_result['effNcar']}");
      // _preferences.setString("gradeUcar", "${_result['gradeUcar']}");
      // _preferences.setString("effUcar", "${_result['effUcar']}");
      // _preferences.setString("fullName", "${_result['fullName']}");
      print("masuk");
      grade = _result['gradeNmcy'] != 'null' && _result['gradeNmcy'] != '-'
          ? _result['gradeNmcy']
          : _result['gradeUmcy'] != 'null' && _result['gradeUmcy'] != '-'
              ? _result['gradeUmcy']
              : _result['gradeNcar'] != 'null' && _result['gradeNcar'] != '-'
                  ? _result['gradeNcar']
                  : _result['gradeUcar'] != 'null' &&
                          _result['gradeUcar'] != '-'
                      ? _result['gradeUcar']
                      : 'Baik';
      portfolio = _result['gradeNmcy'] != 'null' && _result['gradeNmcy'] != '-'
          ? 'Nmcy'
          : _result['gradeUmcy'] != 'null' && _result['gradeUmcy'] != '-'
              ? 'Umcy'
              : _result['gradeNcar'] != 'null' && _result['gradeNcar'] != '-'
                  ? 'Ncar'
                  : _result['gradeUcar'] != 'null' &&
                          _result['gradeUcar'] != '-'
                      ? 'Ucar'
                      : 'Nmcy';
      _preferences.setString("portfolio", portfolio);
      // print(fullName);
      // if (_result['Message'] == 'OK') {
      //     loginProcess = false;
      //     Navigator.of(context).pushReplacement(
      //         MaterialPageRoute(builder: (context) => HomePage()));
      //   } else {
      //     showSnackBar(_result['Message']);
      //   }
    } on TimeoutException catch (_) {
      showSnackBar("Timeout connection");
    } catch (e) {
      print(e);
    }
    this._loadData = false;
    notifyListeners();
  }

  showSnackBar(String text) {
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,
        backgroundColor: snackbarColor,
        duration: Duration(seconds: 2)));
  }
}
