import 'dart:convert';

import 'package:adira_one_partner/services/order_api_provider.dart';
import 'package:adira_one_partner/services/submit_data_konsumen_notifier.dart';
import 'package:adira_one_partner/views/submit_order/nmcy/data_domisili.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';

class SubmitDomisiliNotifier extends ChangeNotifier {
  TextEditingController _controllerAlamatDomisili = TextEditingController();
  TextEditingController _controllerAlamatOcr = TextEditingController();
  TextEditingController _controllerKelurahanAdd = TextEditingController();
  int _rbDomisili = 0;
  bool _checkKelurahan = false;

  // void getDataCustomer(BuildContext context) {
  //   _controllerAlamatOcr =
  //       Provider.of<SubmitDataConsumen>(context, listen: false)
  //           .alamatController;
  // }

  // handleRadioValueChange(int value) {
  //   _rbDomisili = value;
  //   if (_rbDomisili == 0) {
  //     _controllerAlamatDomisili = _controllerAlamatOcr;
  //   } else {
  //     _controllerAlamatDomisili.text = '';
  //   }
  //   notifyListeners();
  // }

  int get rbDomisili => this._rbDomisili;

  set rbDomisili(value) {
    this._rbDomisili = value;
    notifyListeners();
  }

  TextEditingController get controllerAlamatDomisili =>
      _controllerAlamatDomisili;

  set controllerAlamatDomisili(TextEditingController value) {
    this._controllerAlamatDomisili = value;
    notifyListeners();
  }

  TextEditingController get controllerAlamatOcr => _controllerAlamatOcr;

  set controllerAlamatOcr(TextEditingController value) {
    this._controllerAlamatOcr = value;
    notifyListeners();
  }

  TextEditingController get controllerKelurahanAdd => _controllerKelurahanAdd;

  set controllerKelurahanAdd(TextEditingController value) {
    this._controllerKelurahanAdd = value;
    notifyListeners();
  }

  var idKelurahan, idKecamatan, idKota, idKotaProvinsi, zipCode;

  final _controllerKecamatan = TextEditingController();
  final _controllerKelurahan = TextEditingController();
  final _controllerKodePos = TextEditingController();
  final _controllerProvinsi = TextEditingController();
  final _controllerKota = TextEditingController();
  TextEditingController _controllerKelurahanAll = TextEditingController();

  TextEditingController get controllerKelurahanAll => _controllerKelurahanAll;

  set controllerKelurahanAll(TextEditingController value) {
    this._controllerKelurahanAll = value;
    notifyListeners();
  }

  setValueKecamatan(Map value) {
    print("cek kecamatan dkk $value");
    _controllerKelurahanAll.text =
        value['PARA_KELURAHAN_DESC'].toString().trim() +
            ', ' +
            value['PARA_KECAMATAN_DESC'].toString().trim() +
            ', ' +
            value['PROVINSI_DESC'].toString().trim();
    _controllerKota.text = value['KAB_KOT_DESC'];
    _controllerKecamatan.text = value['PARA_KECAMATAN_DESC'];
    _controllerKelurahan.text = value['PARA_KELURAHAN_DESC'];
    _controllerKodePos.text = value['PARA_KELURAHAN_ZIP_CODE'];
    _controllerKota.text = value['KAB_KOT_DESC'];
    _controllerProvinsi.text = value['PROVINSI_DESC'];
    idKecamatan = value['PARA_KELURAHAN_ID_KEC'];
    idKelurahan = value['PARA_KELURAHAN_ID'];
    idKota = value['KAB_KOT_ID'];
    idKotaProvinsi = value['KAB_KOT_ID_PROV'];
    zipCode = value['PARA_KELURAHAN_ZIP_CODE'];
  }

  bool get checkKelurahan => this._checkKelurahan;

  set checkKelurahan(value) {
    this._checkKelurahan = value;
    notifyListeners();
  }

  TextEditingController _controllerJamSurvey = TextEditingController();
  TextEditingController _controllerSelectedDate = TextEditingController();
  TextEditingController _controllerNotifDate = TextEditingController()
    ..text = 'Anda Belum Memilih Jadwal Survey';

  TextEditingController get controllerJamSurvey => _controllerJamSurvey;

  set controllerJamSurvey(TextEditingController value) {
    this._controllerJamSurvey = value;
    notifyListeners();
  }

  TextEditingController get controllerSelectedDate => _controllerSelectedDate;

  set controllerSelectedDate(TextEditingController value) {
    this._controllerSelectedDate = value;
    notifyListeners();
  }

  TextEditingController get controllerNotifDate => _controllerNotifDate;

  set controllerNotifDate(TextEditingController value) {
    this._controllerNotifDate = value;
    notifyListeners();
  }

  void setValueNotif(String param) {
    _controllerJamSurvey.text = param;
    _controllerNotifDate.text = "Anda Memilih " +
        _controllerSelectedDate.text +
        ". Pkl " +
        _controllerJamSurvey.text;

    // Navigator.pop(context);
  }

  void clearDataDomisili() {
    this._controllerAlamatDomisili.clear();
    this._controllerAlamatOcr.clear();
    this._controllerKecamatan.clear();
    this._controllerKelurahan.clear();
    this._controllerKelurahanAdd.clear();
    this._controllerKelurahanAll.clear();
    this._controllerKodePos.clear();
    this._controllerKota.clear();
    this._controllerProvinsi.clear();
    this._controllerJamSurvey.clear();
    this._controllerSelectedDate.clear();
    this._controllerNotifDate.clear();
    this.idKecamatan = null;
    this.idKelurahan = null;
    this.idKota = null;
    this.idKotaProvinsi = null;
    this.zipCode = null;
  }
}
