import 'dart:io';

import 'package:adira_one_partner/widgets/ocr_ktp.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http_parser/http_parser.dart';
import 'package:image/image.dart' as IMG;
import 'dart:math';

class SubmitDataConsumen extends ChangeNotifier {
  TextEditingController nikController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();

  var phoneNoController = TextEditingController();
  var waNoController = TextEditingController();
  var alamatController = TextEditingController();

  bool _checkBox = false;
  bool _waEnable = true;

  File _imageFile;

  cropSquare(String srcFilePath, String destFilePath, bool flip, double width,
      double height) async {
    var bytes = await File(srcFilePath).readAsBytes();
    IMG.Image src = IMG.decodeImage(bytes);

    var w = width.toInt();
    var h = height.toInt();
    /**
     * offsetX -> Untuk Menentukan Batas X
     * offsetY -> Untuk Menentukan Batas Y
     * 
     * jika offsetX dan offsetY di set 0 maka proses Croping akan dimulai 
     * dari titik paling atas pojok layar handphone yaitu (0,0)
     */
    int offsetX = (src.width - max(src.height, src.width)) ~/ 2;
    int offsetY = (src.height - min(src.width, src.height)) ~/ 2;

    IMG.Image destImage = IMG.copyCrop(src, offsetX, offsetY, w, h);

    if (flip) {
      destImage = IMG.flipVertical(destImage);
    }

    var jpg = IMG.encodeJpg(destImage);
    print(destFilePath);
    _imageFile = await File(destFilePath).writeAsBytes(jpg);
    notifyListeners();
  }

  Future<void> UploadFile() async {
    Response<String> response;
    var dio = Dio();
    String path = "https://idpgwuat.adiraku.co.id/ocr/v1/ktp/results";
    String fileName = _imageFile.path.split("/").last;
    try {
      FormData formData = new FormData.fromMap({
        "requestId": "5922441233443",
        "file": await MultipartFile.fromFile(_imageFile.path,
            filename: fileName, contentType: MediaType('image', 'jpg')),
        "type": "image/jpg"
      });
      response = await dio.post(path, data: formData);
      OcrKtp _ocrKtp = ocrKtpFromJson(response.data);

      nikController.text =
          _ocrKtp.ktpData.nik != null ? _ocrKtp.ktpData.nik.value : "000";
      fullNameController.text =
          _ocrKtp.ktpData.nama != null ? _ocrKtp.ktpData.nik.value : "Nama";

      alamatController.text = _ocrKtp.ktpData.alamat != null
          ? _ocrKtp.ktpData.alamat.value
          : "Dummy Alamat";

      print(response.data); //print balikan doang

    } catch (e) {
      print(e.toString());
    } finally {
      dio.close();
    }
  }

  get imageFile => this._imageFile;

  set imageFile(value) {
    this._imageFile = value;
    notifyListeners();
  }

  TextEditingController get getNikController => this.nikController;

  set setNikController(var nikController) {
    this.nikController = nikController;
    notifyListeners();
  }

  TextEditingController get getFullNameController => this.fullNameController;

  set setFullNameController(fullNameController) {
    this.fullNameController = fullNameController;
    notifyListeners();
  }

  get getAlamatController => this.alamatController;

  set setAlamatController(alamatController) {
    this.alamatController = alamatController;
    notifyListeners();
  }

  get getPhoneNoController => this.phoneNoController;

  set setPhoneNoController(phoneNoController) {
    this.phoneNoController = phoneNoController;
  }

  get getWaNoController => this.waNoController;

  set setWaNoController(waNoController) {
    this.waNoController = waNoController;
    notifyListeners();
  }

  bool get checkBox => this._checkBox;

  set checkBox(value) {
    this._checkBox = value;
    notifyListeners();
  }

  bool get waEnable => this._waEnable;

  set waEnable(bool value) {
    this._waEnable = value;
    notifyListeners();
  }

  sameWithPhone() {
    _checkBox == true ? _waEnable = false : _waEnable = true;
  }

  void clearDataCustomer() {
    this.nikController.clear();
    this.fullNameController.clear();
    this.alamatController.clear();
    this.phoneNoController.clear();
    this.waNoController.clear();
  }
}
